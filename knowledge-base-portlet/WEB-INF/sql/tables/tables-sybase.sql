create table KBArticle (
	uuid_ varchar(75) null,
	kbArticleId decimal(20,0) not null primary key,
	resourcePrimKey decimal(20,0),
	groupId decimal(20,0),
	companyId decimal(20,0),
	userId decimal(20,0),
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	rootResourcePrimKey decimal(20,0),
	parentResourceClassNameId decimal(20,0),
	parentResourcePrimKey decimal(20,0),
	kbFolderId decimal(20,0),
	version int,
	title varchar(1000) null,
	urlTitle varchar(75) null,
	content text null,
	description varchar(1000) null,
	priority float,
	sections varchar(1000) null,
	viewCount int,
	latest int,
	main int,
	sourceURL varchar(1000) null,
	status int,
	statusByUserId decimal(20,0),
	statusByUserName varchar(75) null,
	statusDate datetime null
)
go

create table KBComment (
	uuid_ varchar(75) null,
	kbCommentId decimal(20,0) not null primary key,
	groupId decimal(20,0),
	companyId decimal(20,0),
	userId decimal(20,0),
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	classNameId decimal(20,0),
	classPK decimal(20,0),
	content varchar(1000) null,
	userRating int,
	status int
)
go

create table KBFolder (
	uuid_ varchar(75) null,
	kbFolderId decimal(20,0) not null primary key,
	groupId decimal(20,0),
	companyId decimal(20,0),
	userId decimal(20,0),
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	parentKBFolderId decimal(20,0),
	name varchar(75) null,
	urlTitle varchar(75) null,
	description varchar(1000) null
)
go

create table KBTemplate (
	uuid_ varchar(75) null,
	kbTemplateId decimal(20,0) not null primary key,
	groupId decimal(20,0),
	companyId decimal(20,0),
	userId decimal(20,0),
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	title varchar(1000) null,
	content text null
)
go
