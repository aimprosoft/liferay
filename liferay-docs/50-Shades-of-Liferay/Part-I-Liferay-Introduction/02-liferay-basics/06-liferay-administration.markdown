# Liferay Administration [](id=liferay-administration)

For **Liferay** administering we sould go **Admin → Control Panel**.

<p>&nbsp;</p>
### Portal Instances

**Portal instance (or company)** - it is a separate instantiation of the portal. By default, one company **liferay.com** is created in Liferay:

<p class="article-img2"></p>
![Liferay](images/02-43-portal-instance.png)

They are used for differentiation of data (users, organizations, content, etc.) between different companies.

For this purpose, almost in the every liferay-table the field **companyId** exists, that determines the belonging to the company.

Mainly, single company is used (that is created by default).

<p>&nbsp;</p>
### Users and Organizations

In Liferay users are created in **Users** and **Organizations → Add → User**:

<p class="article-img2"></p>
![Liferay](images/02-44-users.png)

Users have the opportunities:
+ to login Liferay
+ to have their own website (set of **public** and/or **private** pages)
+ to be memebers of the organizations/websites/user groups
+ to have certain roles, which give them certain permissions (access rights)

Organization – is Liferay structural unit, uniting the users (users can be organization members).

The organization can have its own website (a set of **public** and/or **private** pages).

Organizations have a hierarchical structure (some organizations can be sub-organizations of the others).

Organizations can be of two types: **Regular Organization** (it is usual organization) and **Location** (it differs from the usual in that has the fields for entering a names of a country and a region).Organizations can be of two types: Regular Organization (it is usual organization) and Location (it differs from the usual in that has the fields for entering a names of a country and a region).

Example organization:

<p class="article-img2"></p>
![Liferay](images/02-45-aim.png)

To add a user to the organization, we should go to **Actions → Assign Users**, then go to the tab **Available** and select the required users and click on **Update Association**:

<p class="article-img2"></p>
![Liferay](02-46-add-user-org.png)

To add a subsidiary, we should press **Add Regular Organization** (or **Add Location**) of the current organization:

<p class="article-img2"></p>
![Liferay](02-47-add-child1.png)

<p class="article-img2"></p>
![Liferay](images/02-48-add-child2.png)

Website of the organization can have its own content (documents, web content, wiki pages, etc.):

<p class="article-img2"></p>
![Liferay](02-49-org-site.png)

<p>&nbsp;</p>
### The User Groups

**The user group** – is also Liferay structural unit, uniting the users.

Users can be user group members.

**User group** can have a set of **public** and **private** pages.

<p class="article-img2"></p>
![Liferay](02-50-user-group.png)

But, unlike the organization, **user group** site can not have the content (documents, web-content, etc.).

**User groups** don't have a hierarchical structure.

<p>&nbsp;</p>
### Roles and Permissions

Roles in Liferay intended for determination of users permissions (access rights) to certain objects (content, pages, portlets, etc.). These roles can be assigned to users to provide them with requredaccess rights.

In Liferay 14 roles have already been created by default:

<p class="article-img2"></p>
![Liferay](02-51-roles.png)

If necessary, we can create our own custom roles.

The roles can be:
+ **Regular Role** (general role) – granted to user for entire portal
+ **Site Role** (role for site) – granted to user for the site
+ **Organization Role** (organizational role) – granted user for the entire organization

The same user can have different roles in different sites/organization. For example, theuser ca be admin in one organization, but at the same time he can be usual user in another one.

To add a role to a user, we should go to **Edit User** and select the required roles:

<p class="article-img2"></p>
![Liferay](02-52-user-roles.png)

To assign the permissions for the role, we should select **Actions → Define Permissions** for this role, then select the required permissions and click on **Save**.

For example, for created role **Organization Publisher** we select the permissions, required for content management.

<p class="article-img2"></p>
![Liferay](02-53-perms.png)

Users with this role will have the appropriate access rights.

