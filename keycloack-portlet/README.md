#KeyCloack Login Hook

**Overview**

KeyCloack Login Hook is an SSO solution for integration KeyCloack server with Liferay.

KeyCloack version: *1.6.1.Final*

Liferay version: *6.2-ce-ga4*


**Description**

Hook intercepts Liferay action, when not signed in user tries to access private page (like *'/group/guest'*) and redirects him to KeyCloack server.

After signing in on KeyCloack user is redirected back to Liferay and logged in automatically (account is also created if user with such email does not exist).

When user signs out from Liferay - he's also signed out from KeyCloack.

**Installation Instructions**

1. Install Liferay 6.2-ce-ga4.
2. Install KeyCloack 1.6.1.Final.
3. Fetch 'keycloak-portlet' code.
4. Modify paths to Liferay in pom.xml.
5. Configure properties (see Configuration Instructions).
6. Run maven package task.

**Configuration Instructions**

Edit settings in *src/main/resourcesportal.properties* file (in *KeyLoack settings* section):

*keycloak.realm.name* - Realm name ;

*keycloak.client.id* - Client Id;

*keycloak.secret* - Client Secret;

*keycloak.base.url* - Base KeyCloack URL;

*keycloak.redirect.url* - Default KeyCloack redirect URL;

*keycloak.default.password*  - Default password for newly created users in Liferay.


Those properties can be also configured in Control Panel on-the-fly:
'Admin -> Control Panel -> Portal Settings -> Authentication -> KeyCloack'





