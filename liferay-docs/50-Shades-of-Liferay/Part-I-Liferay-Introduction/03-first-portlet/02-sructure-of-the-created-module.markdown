# Sructure of the created module [](id=sructure-of-the-created-module)

After creating the module with using *Maven archetype*, we will have as a result the module *'hello-world'* with such structure:

<p class="article-img3"></p>
![Liferay](images/03-05-project-structure.png)


