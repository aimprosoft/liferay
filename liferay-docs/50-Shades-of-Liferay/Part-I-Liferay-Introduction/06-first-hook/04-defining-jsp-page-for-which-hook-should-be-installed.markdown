# Defining JSP-page, for which hook should be installed [](id=defining-JSP-page-for-which-hook-should-be-installed)

One of the main technology that is used in Liferay, is Struts.

Therefore, it is possible to define which *jsp* in which portlet is used via *struts action* (file /ROOT/WEB-INF/struts-config.xml).

To do it, go on *Login* portlet:

<p class="article-img3"></p>
![Liferay](images/06-04-login-portlet.png)

In *FireBug* we examine URL forms.

<p class="article-img3"></p>
![Liferay](images/06-05-url-form.png)

As we see, one of the parameters in this URL is *struts_action=/login/login*. 

We should find the appropriate action in *struts-config.xml*:

<p class="article-img3"></p>
![Liferay](images/06-06-action.png)

We examine *path* for *forward*:

*path="portlet.login.login"*

Using this *path* we should find the required *jsp* in file /ROOT/WEB-INF/tiles-defs.xml:

<p class="article-img3"></p>
![Liferay](images/06-07-definition.png)

That is, required *jsp* for hook - is /portlet/login/login.jsp.


