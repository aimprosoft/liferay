package com.aimprosoft.keycloack.invoker.impl;

import com.aimprosoft.keycloack.invoker.KeyCloackInvoker;
import com.aimprosoft.keycloack.util.KeyCloackUtils;
import org.apache.http.NameValuePair;

import java.util.List;

public class AuthInvoker extends KeyCloackInvoker {

    public AuthInvoker(long companyId) {
        super(companyId);
    }

    public AuthInvoker(long companyId, String redirectUrl) {
        super(companyId, redirectUrl);
    }

    @Override
    public String buildInvokeUrl() {
        
        return KeyCloackUtils.getAuthUrl(getCompanyId(), getRedirectUrl());
    }

    @Override
    public void fillFormParams(List<NameValuePair> nameValuePair) {

    }

}
