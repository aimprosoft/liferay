# First Startup [](id=first-startup)

We have already downloaded Liferay files, now we are starting it.

Go to **tomcat/bin** folder inside unpacked Liferay (**liferay-portal-6.2-ce-ga4/tomcat-7.0.42/bin**).

To start Liferay on Windows just run **'startup.bat'** file, and see logs in console output.

To start Liferay on Linux, run from console:

`./startup.sh`

then go to **tomcat-7.0.42/logs** directory and watch logs:

`tail -f catalina.out`

After normal startup the following message should appear in logs: **INFO: Server startup in [...] ms**

<p class="article-img2"></p>
![Liferay](images/01-08-tomcat-ok.jpg)

After starting server open this URL in browser: http://localhost:8080 (if it has not opened automatically).
You'll be prompted to configure Liferay portal:

<p class="article-img1"></p>
![Liferay](images/01-09-config.png)

Leave everything as default here, remove **Add Sample Data** flag to speed-up configuration process,
press **Finish Configuration**. After this click **Go to My Portal**, agree with **Terms of Use**, create new password for user, 
answer reminder question, and you'll be redirected to Liferay home page:

<p class="article-img1"></p>
![Liferay](images/01-11-home-page.jpg)

Now you're done, Liferay is started up and configured. You may use it.


