# Download Liferay [](id=download-liferay)

<p>&nbsp;</p>
To download Liferay visit Liferay's official site: [https://www.liferay.com](https://www.liferay.com).

Click **Download**, then choose **Bundled with Tomcat**, click **Download** again:

<p class="article-img1"></p>
![Liferay](images/01-01-download.jpg)

After clicking **Download** the latest stable Liferay version will be downloaded automatically (after 5 seconds).

To choose the version you need, and also to download sources and documentation, you may click on **Liferay Portal**
link (without waiting for automatic downloading):

<p class="article-img2"></p>
![Liferay](images/01-02-liferay-portal.jpg)

then click **Files** link:

<p class="article-img2"></p>
![Liferay](images/01-03-files.jpg)

then click **Liferay Portal** link:

<p class="article-img2"></p>
![Liferay](images/01-04-files-portal.jpg)

Now choose the Liferay version (the latest stable one is 6.2-ce-ga4 at the moment):

<p class="article-img2"></p>
![Liferay](images/01-05-liferay-version.jpg)
    
You'll be redirected to this page [http://sourceforge.net/projects/lportal/files/Liferay Portal/6.2.3 GA4/](http://sourceforge.net/projects/lportal/files/Liferay%20Portal/6.2.3%20GA4/). 

Now you need to download **Liferay** (1), **Liferay sources** (2) and **Liferay documentation** (3) from here:

<p class="article-img2"></p>
![Liferay](images/01-06-files-to-download.jpg)

You'll get the following files in **Downloads**:

<p class="article-img2"></p>
![Liferay](images/01-07-downloaded-files.jpg)

Copy those files to your Liferay's directory (**D:/Work/Liferay/[Project-name]** on **Windows**, **/home/{user}/Work/Liferay/[Project-name]** on **Linux**),
and unpack Liferay (**liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip**).

Now you have all required Liferay files and you're ready to start it.


