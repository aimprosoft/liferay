# Which hooks do exist? [](id=which-hooks-do-exist)

Types of hooks:

+ on **portal.properties** - for redefinition properties of the portal (which are specified in /ROOT/WEB-INF/lib/portal-impl.jar!/portal.properties). Not all portal properties can be re-specified via hook, it is possible only for described in /ROOT/dtd/liferay-hook_6_2_0.dtd properties
+ on **language-properties** - for redefinition properties in *Resource Bundle* (/ROOT/WEB-INF/lib/portal-impl.jar!/content/Language.properties)
+ on **indexer** (*indexer-post-processor*)
+ on **service**
+ on **struts action**
+ on **servlet-filter**
+ on **jsp** page (jsp-hook) - it allows us to change Liferay jsp-page

In this section we will create hook on jsp-page (out of login portlet).


