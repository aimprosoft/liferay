# Liferay's Page Structure [](id=page-structure)

After starting Liferay server and going to it's home page, you see, that it has the following page structure:

<p>&nbsp;</p>
<p class="article-img1"></p>
![Liferay](images/02-01-liferay-page.png)

As you see from here, Liferay page consists from the following parts:

1) **DockBar** - contains menu for Liferay administration, site navigation menu, notifications menu,
profile management menu.

2) **MenuBar** - menu for content management. Provides capabilities for adding/editing pages,
and also applying different themes/layouts for pages, adding portlets to pages, page permissions management, etc.

3) **NavigationBar** - navigation menu. It contains pages and sub-pages (only 1st level by default) for the current site.
Only those pages are displayed in this menu, which are not marked as 'hidden', and on which current user has sufficient permissions.

4) **Portlets** - separate functional modules in Liferay, which may be added to a page, and which represent a part of portal's page view.
Each portlet may have different permissions settings, so users with different roles may see different portlets on the same portal pages.


