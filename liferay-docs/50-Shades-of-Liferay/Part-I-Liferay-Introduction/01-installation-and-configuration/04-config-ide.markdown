# IDE Configuration [](id=ide-config)

We have already started Liferay and made some configuration enhancements for it.
But we were starting it from console. And it's not good for the development process,
as you actually need to debug your code. 
So, we'll need to configure our Liferay in **IDE** and run it from inside **IDE** in **Debug mode**.
This section explains how to do this.

<p>&nbsp;</p>
You may use different IDEs for Liferay development.

Some people prefer Liferay IDE, based on **Eclipse** (https://www.liferay.com/downloads/liferay-projects/liferay-ide).
It simplifies Liferay development and deployment process, and has special plugins for portlets/themes development, etc.
But I don't use it, as I don't like Eclipse products at all.

Other people use **NetBeans** or some other IDEs.

Personally I prefer using **Intellij IDEA** (https://www.jetbrains.com/idea/).
Although it's not Liferay-specific, development process is pretty clean and simple inside it.

Now I'll explain how to configure Liferay in Intellij IDEA IDE.

<p>&nbsp;</p>
### Download IDEA

If you still don't have IDEA installed, download it from here: https://www.jetbrains.com/idea/download/
(the latest version is **14.1** at this moment).
Download **Ultimate Edition** (as **Community Edition** doesn't have enough features for development):

<p class="article-img2"></p>
![Liferay](images/01-14-idea-download.jpg)

Keep in mind, that it's paid product.

<p>&nbsp;</p>
### Create new project

Start IntelliJ IDEA and create new project in it:

<p class="article-img2"></p>
![Liferay](images/01-15-new-project-1.jpg)

Click **Next**, specify project name, and click **Finish**:

<p class="article-img2"></p>
![Liferay](images/01-16-new-project-2.jpg)

You're done, new project has been created.

<p>&nbsp;</p>
### Create new Application Server in 'Settings' menu

Now we'll create new application server for our Liferay in IDEA.
Press **Settings** button (1) in Toolbar, and create new Tomcat server, as illustrated here:

<p class="article-img2"></p>
![Liferay](images/01-17-app-server-1.jpg)

Now choose tomcat folder inside Downloaded Liferay:

<p class="article-img2"></p>
![Liferay](images/01-18-app-server-2.jpg)

Specify name fow newly created application server (based on project name), and press **Ok**:

<p class="article-img2"></p>
![Liferay](images/01-19-app-server-3.jpg)

Application server should have been created after these steps.

<p>&nbsp;</p>
### Configure Server in 'Run/Debug Configurations' menu

Now we'll configure our Liferay server in IDEA.
Go to **Run -> Edit Configuration...** menu (or press appropriate icon in Toolbar), and add new Tomcat server:

<p class="article-img2"></p>
![Liferay](images/01-20-server-1.jpg)

Specify server name (based on project name), and choose **Application server** (created on previous step):

<p class="article-img2"></p>
![Liferay](images/01-21-server-2.jpg)

On **Startup/Connection** tab (1) add (3) **CATALINA_BASE** (4) Environment variable with path to Liferay's tomcat folder (5)
for **Run and Debug** (2). Then press **Ok** (6):

<p class="article-img2"></p>
![Liferay](images/01-22-server-3.jpg)

Basic Liferay configuration in IDEA is finished at this point, and you're ready to start it.

<p>&nbsp;</p>
### Startup Liferay from IDEA

Start Liferay in Debug mode from IDEA. Press **Debug** button for this (or press **Shift+F9** hotkey):

<p class="article-img4"></p>
![Liferay](images/01-23-debug.jpg)

Watch logs in **Output** window in IDEA, make sure everything is Ok, and start development with Liferay.


