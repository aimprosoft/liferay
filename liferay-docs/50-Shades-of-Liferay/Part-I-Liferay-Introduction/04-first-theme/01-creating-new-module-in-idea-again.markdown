# Creating new module in IDEA [](id=creating-new-module-in-IDEA-again)

We will create the new module *'aimprosoft'* with using of *'Maven archetype'.* 
To do it, go *File → New Module*..., select *Maven*, check *'Create from archetype'* and select *'liferay-theme-archetype'*:

<p class="article-img2"></p>
![Liferay](images/04-01-theme.png)

Specify *groupld* and *artifactld*:

<p class="article-img2"></p>
![Liferay](images/04-02-theme2.png)

Check *Maven settings*, specify location of module with the theme.

Structure of the theme will be created automatically.

We will create a theme on the basis of standard *Liferay classic* theme. To do it, copy all files _*.vm_ from _ROOT/html/themes/classic/templates_, and also copy *js, css, images* folders. We will get such structure:

<p class="article-img3"></p>
![Liferay](images/04-03-theme-structure.png)

After compiling we get the ready theme, the same as the *classic* one.

We will change:

+ layout in *portal_normal.vm*
+ styles in *custom.css*
+ images *favicon.png* and *thumbnail.png*
+ of necessity *navigation.vm* (for changing of navigation)
+ *portlet.vm* (for changing of portlets)

In *init_custom.vm* we can specify our own variables, which we will be able to use later in *portal_normal.vm*. List of all predefined variables is located in the file */ROOT/html/themes/_unstyled/templates/init.vm*.

After necessary changes we should recompile the theme and apply it to necessary pages.


