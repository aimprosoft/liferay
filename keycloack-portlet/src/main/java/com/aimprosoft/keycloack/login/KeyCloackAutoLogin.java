package com.aimprosoft.keycloack.login;

import com.aimprosoft.keycloack.model.KeyCloakUser;
import com.aimprosoft.keycloack.util.KeyCloakValues;
import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.BaseAutoLogin;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;

public class KeyCloackAutoLogin extends BaseAutoLogin {

    @Override
    protected String[] doLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {

        _log.info("com.aimprosoft.keycloack.custom.login.KeyCloackAutoLogin.doLogin invoked. ");
        
        HttpSession session = request.getSession();
        KeyCloakUser keyCloackUser = (KeyCloakUser) session.getAttribute("keyCloackUser");
        
        if (keyCloackUser == null || "".equals(keyCloackUser.getEmail())) {
            return null;
        }
        
        try {

            long companyId = PortalUtil.getCompanyId(request);
            Locale locale = PortalUtil.getLocale(request);
            String email = keyCloackUser.getEmail();

            User userToLogin;
            try {
                userToLogin = UserLocalServiceUtil.getUserByEmailAddress(companyId, email);
            } catch (NoSuchUserException nsue) {
                userToLogin = null;
            }

            if (userToLogin == null) {

                //add new user
                long creatorUserId = 0;
                boolean autoPassword = false;
                String defaultPassword = KeyCloakValues.DEFAULT_PASSWORD;
                boolean autoScreenName = true;
                String screenName = keyCloackUser.getPreferredUserName();
                long faceBookId = -1;
                String openId = "";
                String firstName = keyCloackUser.getGivenName();
                String middleName = "";
                String lastName = keyCloackUser.getFamilyName();
                int prefixId = -1;
                int suffixId = -1;
                boolean male = true;
                int birthdayMonth = 0;
                int birthdayDay = 1;
                int birthdayYear = 1970;
                String jobTitle = "";
                long[] groupIds = {};
                long[] organizationIds = {};
                long[] roleIds = {};
                long[] userGroupIds = {};
                boolean sendEmail = false;
                ServiceContext serviceContext = new ServiceContext();

                userToLogin = UserLocalServiceUtil.addUser(
                        creatorUserId,
                        companyId,
                        autoPassword,
                        defaultPassword,
                        defaultPassword,
                        autoScreenName,
                        screenName,
                        email,
                        faceBookId,
                        openId,
                        locale,
                        firstName,
                        middleName,
                        lastName,
                        prefixId,
                        suffixId,
                        male,
                        birthdayMonth,
                        birthdayDay,
                        birthdayYear,
                        jobTitle,
                        groupIds,
                        organizationIds,
                        roleIds,
                        userGroupIds,
                        sendEmail,
                        serviceContext
                );
            }

            if (userToLogin != null) {
                String[] credentials = new String[3];

                credentials[0] = String.valueOf(userToLogin.getUserId());
                credentials[1] = userToLogin.getPassword();
                credentials[2] = Boolean.FALSE.toString();

                session.removeAttribute("keyCloackUser");
                
                return credentials;    
            }

            

        } catch (Exception e) {
            _log.error("Error in KeyCloackAutoLogin: " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    private static Log _log = LogFactoryUtil.getLog(KeyCloackAutoLogin.class);
}
