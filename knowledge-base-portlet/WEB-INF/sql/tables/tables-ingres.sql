create table KBArticle (
	uuid_ varchar(75) null,
	kbArticleId bigint not null primary key,
	resourcePrimKey bigint,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	rootResourcePrimKey bigint,
	parentResourceClassNameId bigint,
	parentResourcePrimKey bigint,
	kbFolderId bigint,
	version integer,
	title varchar(1000) null,
	urlTitle varchar(75) null,
	content long varchar null,
	description varchar(1000) null,
	priority float,
	sections varchar(1000) null,
	viewCount integer,
	latest tinyint,
	main tinyint,
	sourceURL varchar(1000) null,
	status integer,
	statusByUserId bigint,
	statusByUserName varchar(75) null,
	statusDate timestamp null
);

create table KBComment (
	uuid_ varchar(75) null,
	kbCommentId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	classNameId bigint,
	classPK bigint,
	content varchar(1000) null,
	userRating integer,
	status integer
);

create table KBFolder (
	uuid_ varchar(75) null,
	kbFolderId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	parentKBFolderId bigint,
	name varchar(75) null,
	urlTitle varchar(75) null,
	description varchar(1000) null
);

create table KBTemplate (
	uuid_ varchar(75) null,
	kbTemplateId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	title varchar(1000) null,
	content long varchar null
);
