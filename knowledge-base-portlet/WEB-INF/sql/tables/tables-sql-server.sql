create table KBArticle (
	uuid_ nvarchar(75) null,
	kbArticleId bigint not null primary key,
	resourcePrimKey bigint,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName nvarchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	rootResourcePrimKey bigint,
	parentResourceClassNameId bigint,
	parentResourcePrimKey bigint,
	kbFolderId bigint,
	version int,
	title nvarchar(2000) null,
	urlTitle nvarchar(75) null,
	content nvarchar(max) null,
	description nvarchar(2000) null,
	priority float,
	sections nvarchar(2000) null,
	viewCount int,
	latest bit,
	main bit,
	sourceURL nvarchar(2000) null,
	status int,
	statusByUserId bigint,
	statusByUserName nvarchar(75) null,
	statusDate datetime null
);

create table KBComment (
	uuid_ nvarchar(75) null,
	kbCommentId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName nvarchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	classNameId bigint,
	classPK bigint,
	content nvarchar(2000) null,
	userRating int,
	status int
);

create table KBFolder (
	uuid_ nvarchar(75) null,
	kbFolderId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName nvarchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	parentKBFolderId bigint,
	name nvarchar(75) null,
	urlTitle nvarchar(75) null,
	description nvarchar(2000) null
);

create table KBTemplate (
	uuid_ nvarchar(75) null,
	kbTemplateId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName nvarchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	title nvarchar(2000) null,
	content nvarchar(max) null
);
