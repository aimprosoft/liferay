# Introduction [](id=introduction)

                                                                                                    Nothing better in the world,
                                                                                                    Than implementing Liferay code...

<p>&nbsp;</p>
### Why "50 shades"?

Because when you start Liferay development, it seems perversion and masochism for you,
but once you get used to it, things become better.

<p>&nbsp;</p>
### Why Liferay?

Before you start development of new site, you ask yourself - which tool to use for this?
You can either create new site from scratch, or use some ready solutions (CMS).

If you create site from scratch, you spend time for implementing things, which have already been actually implemented: authentication, roles/permissions mechanism, content management, etc. 
They are already integrated into existing CMS, tested and are being successfully used by many companies.
In this case, you "re-invent a bicycle" yourself.

If you're using CMS, you have less freedom and you're forced to use some conventions that are being used there,
but at the same you have a lot of built-in functionality, which you may use in your project.

Personally I prefer 2nd way, especially for large systems development.
But making choice which CMS to use should be also responsible, and project requirements should be taken into account.

After analyzing some existing CMS on Java (Liferay Portal, Magnolia, OpenCMS, Apache Lenya),
we decided, that Liferay is the best option for enterprise portal development.

<p>&nbsp;</p>
It provides developers the following features:

- **authentication** (with ability to configure LDAP/Facebook sign-in, etc.)

- **content aggregation** (web-page is generated from separate independent parts - so-called 'portlets', which are assembled together into a single page)

- **roles and permissions management**

- **personalization** - due to content aggregation and roles/permissions management one and the same page can be shown differently for different users

- **dynamic content management** - portlets can be added to a page on-the-fly, dropped or moved without code changes and server restarts

- **content separation** - content in Liferay belongs to some site inside a portal, which restricts access to it for site members only

- a lot of **ready-for-use portlets** - like Web Content, Wiki, Blog, Document Library, Message Boards, Asset Publisher, etc.

- **powerful administration tools**


