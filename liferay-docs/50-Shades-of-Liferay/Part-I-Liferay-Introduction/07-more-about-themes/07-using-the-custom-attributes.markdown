# Using the custom attributes [](id=using-the-custom-attributes)

Custom attribute can be gained in the topic via **expandoBridge**:

**$entity.getExpandoBridge().getAttribute("[ATTR_NAME]")**,

where **$entity** - an entity, of it attribute we should receive (it can be user, organization, page, etc.), **"[ATTR_NAME]"** - the attribute name.

For the page we create custom attribute **"bgColor"** (for setting the page color in the navigation menu):

<p class="article-img3"></p>
![Liferay](images/07-38-cf1.png)

<p class="article-img2"></p>
![Liferay](images/07-39-cf2.png)

Now, in the **'Manage Pages'** we can specify the value of a custom attribute for the different pages:

<p class="article-img2"></p>
![Liferay](images/07-40-cf3.png)

Then, we change the navigation using a custom attribute for the background color:

<p class="article-img2"></p>
![Liferay](images/07-41-nav-color.png)

After that we will get an multicolored menu:

<p class="article-img2"></p>
![Liferay](images/07-42-color-menu.png)


