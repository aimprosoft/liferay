package com.aimprosoft.keycloack.events;

import com.aimprosoft.keycloack.invoker.impl.LogoutInvoker;
import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class KeyCloakLogoutAction extends Action {

    private static Log _log = LogFactoryUtil.getLog(KeyCloakLogoutAction.class);
    
    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {

        try {
            long companyId = PortalUtil.getCompanyId(request);
            LogoutInvoker logoutInvoker = new LogoutInvoker(companyId);
            String keyCloackLogoutUrl = logoutInvoker.buildInvokeUrl();
            response.sendRedirect(keyCloackLogoutUrl);

        } catch (Exception e) {
            _log.error("Can not logout from KeyCloack: " + e.getMessage());
        }
        
    }
}
