# Liferay Configuration [](id=liferay-config)

We have already started up Liferay with the default configuration settings.
However, this configuration has the following disadvantages for the real projects:

- not enough allocated memory

- working with built-in database (Hypersonic)

Now we'll fix it.

<p>&nbsp;</p>
### Adding allocated memory

Liferay is quite resource-consuming portal, that's why we need to add allocated memory size.
To do this edit **setenv.bat** file on Windows (or **setenv.sh** on Linux) and change values for **-Xmx** and **-XX:MaxPermSize** parameters:

<p class="article-img2"></p>
![Liferay](images/01-12-setenv.jpg)

If you have enough RAM, it's recommended to set those values to **4096m**, otherwise - to **2048m**. Less values are not recommended.

<p>&nbsp;</p>
### Configuring database

Liferay works with built-in Hypersonic database by default.
It's normally to use it for demo projects, but it's not recommended to use it on production.
To change database settings - edit Liferay's config file **liferay-portal-6.2-ce-ga4/portal-setup-wizard.properties**, and add
the following properties:

`jdbc.default.driverClassName=com.mysql.jdbc.Driver`

`jdbc.default.url=jdbc:mysql://localhost:3306/[DB_NAME]`

`jdbc.default.username=root`

`jdbc.default.password=[PASSWORD]`

where [DB_NAME] - database name, [PASSWORD] - MySQL password.

Your config file will be like this:

<p class="article-img2"></p>
![Liferay](images/01-13-db-config.jpg)

**NOTE:** 

Make sure, you have specified correct values for **database name** (1) and **MySQL password** (2).

Restart Liferay to apply those changes.


