package com.aimprosoft.keycloack.events;

import com.aimprosoft.keycloack.invoker.impl.AuthInvoker;
import com.aimprosoft.keycloack.util.KeyCloackUtils;
import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class KeyCloakLoginAction extends Action {

    private static Log _log = LogFactoryUtil.getLog(KeyCloakLogoutAction.class);
    
    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {

        _log.info("KeyCloakLoginAction start.");

        try {

            if (isLoginPortlet(request)) {

                long companyId = PortalUtil.getCompanyId(request);

                String keyCloackRedirect = KeyCloackUtils.getRedirectUrl(companyId);
                HttpSession session = request.getSession();
                if (session.getAttribute("keyCloackRedirect") != null) {
                    keyCloackRedirect = String.valueOf(session.getAttribute("keyCloackRedirect"));
                }
                
                AuthInvoker authInvoker = new AuthInvoker(companyId, keyCloackRedirect);
                String authInvokerUrl = authInvoker.buildInvokeUrl();
                _log.info("KeyCloack auth URL: " + authInvokerUrl);

                response.sendRedirect(authInvokerUrl);
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        _log.info("KeyCloakLoginAction end.");
    }


    private static boolean isLoginPortlet(HttpServletRequest request) {
        String currentURL = PortalUtil.getCurrentURL(request);
        String loginUrl = PortalUtil.getPathMain().concat("/portal/login");
        return currentURL.contains(loginUrl);
    }
    
    
}
