package com.aimprosoft.keycloack.util;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import org.keycloak.common.util.KeycloakUriBuilder;
import org.keycloak.constants.ServiceUrlConstants;

import java.net.URI;

public class KeyCloackUtils {

    public static String getRealmName(long companyId){
        String realmName = KeyCloakValues.REALM_NAME;
        try {
            realmName = PrefsPropsUtil.getString(companyId, KeyCloakKeys.REALM_NAME, KeyCloakValues.REALM_NAME);
        } catch (SystemException e) {
            _log.error("Can not read property + '" + KeyCloakKeys.REALM_NAME + "', cause: " + e.getMessage());
            e.printStackTrace();
        }
        return realmName;
    }

    public static String getClientId(long companyId){
        String clientId = KeyCloakValues.CLIENT_ID;
        try {
            clientId = PrefsPropsUtil.getString(companyId, KeyCloakKeys.CLIENT_ID, KeyCloakValues.CLIENT_ID);
        } catch (SystemException e) {
            _log.error("Can not read property + '" + KeyCloakKeys.CLIENT_ID + "', cause: " + e.getMessage());
            e.printStackTrace();
        }
        return clientId;
    }

    public static String getClientSecret(long companyId){
        String clientSecret = KeyCloakValues.CLIENT_SECRET;
        try {
            clientSecret = PrefsPropsUtil.getString(companyId, KeyCloakKeys.CLIENT_SECRET, KeyCloakValues.CLIENT_SECRET);
        } catch (SystemException e) {
            _log.error("Can not read property + '" + KeyCloakKeys.CLIENT_SECRET + "', cause: " + e.getMessage());
            e.printStackTrace();
        }
        return clientSecret;
    }

    public static String getBaseUrl(long companyId){
        String baseUrl = KeyCloakValues.BASE_URL;
        try {
            baseUrl = PrefsPropsUtil.getString(companyId, KeyCloakKeys.BASE_URL, KeyCloakValues.BASE_URL);
        } catch (SystemException e) {
            _log.error("Can not read property + '" + KeyCloakKeys.BASE_URL + "', cause: " + e.getMessage());
            e.printStackTrace();
        }
        return baseUrl;
    }

    public static String getRedirectUrl(long companyId){
        String redirectUrl = KeyCloakValues.REDIRECT_URL;
        try {
            redirectUrl = PrefsPropsUtil.getString(companyId, KeyCloakKeys.REDIRECT_URL, KeyCloakValues.REDIRECT_URL);
        } catch (SystemException e) {
            _log.error("Can not read property + '" + KeyCloakKeys.REDIRECT_URL + "', cause: " + e.getMessage());
            e.printStackTrace();
        }
        return redirectUrl;
    }

    public static String getDefaultPassword(long companyId){
        String defaultPassword = KeyCloakValues.DEFAULT_PASSWORD;
        try {
            defaultPassword = PrefsPropsUtil.getString(companyId, KeyCloakKeys.DEFAULT_PASS, KeyCloakValues.DEFAULT_PASSWORD);
        } catch (SystemException e) {
            _log.error("Can not read property + '" + KeyCloakKeys.DEFAULT_PASS + "', cause: " + e.getMessage());
            e.printStackTrace();
        }
        return defaultPassword;
    }

    public static String getAuthUrl(long companyId, String redirectUri) {

        String baseUrl = getBaseUrl(companyId);
        String realmName = getRealmName(companyId);
        String clientId = getClientId(companyId);

        URI loginURI = KeycloakUriBuilder.fromUri(baseUrl + "/auth")
                .path(ServiceUrlConstants.AUTH_PATH)
                .build(realmName);
        String loginUrl = loginURI.toString();

        loginUrl += "?client_id=" + clientId;
        loginUrl += "&response_type=code";
        loginUrl += "&redirect_uri=" + redirectUri;

        return loginUrl;

    }

    public static String getAuthUrl(long companyId) {
        String redirectUrl = getRedirectUrl(companyId);
        return getAuthUrl(companyId, redirectUrl);
    }


    public static String getLogoutUrl(long companyId, String redirectUri) {

        String baseUrl = getBaseUrl(companyId);
        String realmName = getRealmName(companyId);
        String clientId = getClientId(companyId);

        URI logoutURI = KeycloakUriBuilder.fromUri(baseUrl + "/auth")
                .path(ServiceUrlConstants.TOKEN_SERVICE_LOGOUT_PATH)
                .build(realmName);

        String logoutUrl = logoutURI.toString();

        logoutUrl += "?client_id=" + clientId;
        logoutUrl += "&response_type=code";
        logoutUrl += "&redirect_uri=" + redirectUri;

        return logoutUrl;
    }

    public static String getLogoutUrl(long companyId) {

        String redirectUrl = getRedirectUrl(companyId);
        
        return getLogoutUrl(companyId, redirectUrl);
    }


    public static String getTokenUrl(long companyId, String redirectUri) {

        String baseUrl = getBaseUrl(companyId);
        String realmName = getRealmName(companyId);

        URI tokenURI = KeycloakUriBuilder.fromUri(baseUrl + "/auth")
                .path(ServiceUrlConstants.TOKEN_PATH)
                .build(realmName);

        return tokenURI.toString();
    }

    public static String getTokenUrl(long companyId) {

        String redirectUrl = getRedirectUrl(companyId);

        return getTokenUrl(companyId, redirectUrl);
    }


    public static String getUserInfoUrl(long companyId, String redirectUri) {

        String baseUrl = getBaseUrl(companyId);
        String realmName = getRealmName(companyId);

        URI userInfoURI = KeycloakUriBuilder.fromUri(baseUrl + "/auth")
                .path("/realms/{realm-name}/protocol/openid-connect/userinfo")
                .build(realmName);

        return userInfoURI.toString();
    }

    public static String getUserInfoUrl(long companyId) {

        String redirectUrl = getRedirectUrl(companyId);

        return getUserInfoUrl(companyId, redirectUrl);
    }

    private static Log _log = LogFactoryUtil.getLog(KeyCloackUtils.class);
}
