package com.aimprosoft.keycloack.util;

import com.liferay.portal.kernel.util.PropsUtil;
import org.keycloak.common.util.KeycloakUriBuilder;
import org.keycloak.constants.ServiceUrlConstants;

import java.net.URI;

public class KeyCloakValues {

    public static final String REALM_NAME           = PropsUtil.get(KeyCloakKeys.REALM_NAME);
    
    public static final String CLIENT_ID            = PropsUtil.get(KeyCloakKeys.CLIENT_ID);

    public static final String CLIENT_SECRET        = PropsUtil.get(KeyCloakKeys.CLIENT_SECRET);
    
    public static final String BASE_URL             = PropsUtil.get(KeyCloakKeys.BASE_URL);

    public static final String REDIRECT_URL         = PropsUtil.get(KeyCloakKeys.REDIRECT_URL);

    public static final String DEFAULT_PASSWORD     = PropsUtil.get(KeyCloakKeys.DEFAULT_PASS);

}
