create table KBArticle (
	uuid_ varchar(75) null,
	kbArticleId bigint not null primary key,
	resourcePrimKey bigint,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	rootResourcePrimKey bigint,
	parentResourceClassNameId bigint,
	parentResourcePrimKey bigint,
	kbFolderId bigint,
	version int,
	title longvarchar null,
	urlTitle varchar(75) null,
	content longvarchar null,
	description longvarchar null,
	priority double,
	sections longvarchar null,
	viewCount int,
	latest bit,
	main bit,
	sourceURL longvarchar null,
	status int,
	statusByUserId bigint,
	statusByUserName varchar(75) null,
	statusDate timestamp null
);

create table KBComment (
	uuid_ varchar(75) null,
	kbCommentId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	classNameId bigint,
	classPK bigint,
	content longvarchar null,
	userRating int,
	status int
);

create table KBFolder (
	uuid_ varchar(75) null,
	kbFolderId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	parentKBFolderId bigint,
	name varchar(75) null,
	urlTitle varchar(75) null,
	description longvarchar null
);

create table KBTemplate (
	uuid_ varchar(75) null,
	kbTemplateId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	title longvarchar null,
	content longvarchar null
);
