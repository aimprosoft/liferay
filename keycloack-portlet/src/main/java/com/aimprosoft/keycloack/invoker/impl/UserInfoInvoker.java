package com.aimprosoft.keycloack.invoker.impl;

import com.aimprosoft.keycloack.invoker.KeyCloackInvoker;
import com.aimprosoft.keycloack.model.KeyCloakUser;
import com.aimprosoft.keycloack.util.KeyCloackUtils;
import com.aimprosoft.keycloack.util.KeyCloakConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.List;

public class UserInfoInvoker extends KeyCloackInvoker {

    private String accessToken;

    public UserInfoInvoker(long companyId, String accessToken) {
        super(companyId);
        this.accessToken = accessToken;
    }

    public UserInfoInvoker(long companyId, String redirectUrl, String accessToken) {
        super(companyId, redirectUrl);
        this.accessToken = accessToken;
    }

    @Override
    public String buildInvokeUrl() {

        return KeyCloackUtils.getUserInfoUrl(getCompanyId(), getRedirectUrl());
    }

    @Override
    public void fillFormParams(List<NameValuePair> formParams) {

        String clientId = KeyCloackUtils.getClientId(getCompanyId());
        String clientSecret = KeyCloackUtils.getClientSecret(getCompanyId());
        
        formParams.add(new BasicNameValuePair(KeyCloakConstants.ACCESS_TOKEN, accessToken));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.GRANT_TYPE, KeyCloakConstants.AUTHORIZATION_CODE));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.REDIRECT_URI, getRedirectUrl()));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.CLIENT_ID, clientId));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.CLIENT_SECRET, clientSecret));
    }

    public KeyCloakUser fetchKeyCloakUser() throws Exception {

        String responseString = invokeKeyCloackService();
        _log.info("responseString: " + responseString);
        
        JSONObject jsonObject = new JSONObject(responseString);

        String email = getJsonValue(jsonObject, KeyCloakConstants.EMAIL);
        String name = getJsonValue(jsonObject, KeyCloakConstants.NAME);
        String familyName = getJsonValue(jsonObject, KeyCloakConstants.FAMILY_NAME);
        String preferredUserName = getJsonValue(jsonObject, KeyCloakConstants.PREFERRED_USERNAME);
        String givenName = getJsonValue(jsonObject, KeyCloakConstants.GIVEN_NAME);
        
        KeyCloakUser keyCloakUser = new KeyCloakUser();
        keyCloakUser.setEmail(email);
        keyCloakUser.setName(name);
        keyCloakUser.setFamilyName(familyName);
        keyCloakUser.setPreferredUserName(preferredUserName);
        keyCloakUser.setGivenName(givenName);

        return keyCloakUser;
    }

    private String getJsonValue(JSONObject jsonObject, String name) {
        try {
            return jsonObject.get(name) != null ? jsonObject.get(name).toString() : StringPool.BLANK;
        } catch (Exception e) {
            _log.error("Can not get json property: " + name);
            return StringPool.BLANK;
        }
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    private static Log _log = LogFactoryUtil.getLog(UserInfoInvoker.class);
}
