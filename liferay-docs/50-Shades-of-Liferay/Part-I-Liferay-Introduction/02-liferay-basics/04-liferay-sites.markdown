# Liferay Sites [](id=liferay-sites)

<p>&nbsp;</p>
### What is Liferay site?

**Liferay Site** (in earlier Liferay versions called **Community**) is one of the fundamental term in Liferay.
From the page management point of view it's a set of public and private pages (see previous section).
Except pages, site has also proper content (Web Content articles, Document and Media documents, Wiki pages, etc.),
and own members (user, user groups or organizations).

<p>&nbsp;</p>
### Default sites in Liferay

There are two pre-defined sites in Liferay by default.
When we were working with page management in previous section - we were working with Liferay's default site - **'Liferay'**.
There is also another site **'Global'**. When you go to **'Sites'** menu inside **Control Panel**, you'll see them:

<p class="article-img2"></p>
![Liferay](images/02-27-sites.jpg)

Here are differences between them:

<p class="article-table"></p>
| **Liferay site**                                   |   **Global Site**                                              |
|:---------------------------------------------------|:---------------------------------------------------------------|
| It has own pages and members of the site           |   It doesn't have pages and members                            |
| Site content is accessible only within this site   |   Site content is global, i.e. accessible from any other site  |

<p>&nbsp;</p>
Default **Liferay** site may be used in portals, which don't have content/pages separation between different parts of portal (sites).
So, if your portal will have only one site - you may use the pre-defined **Liferay** site.

**Global** site is used to store shared content. Content in Liferay (web content, documents, blogs, etc.) belongs to some site inside portal.
By default this content is accessible only inside this site. If you want to make some content shared between different sites 
- you may upload it to **Global** site, and then refer to it from other sites.

<p>&nbsp;</p>
### Sites management

Sites can be created:
+ for **organization** - herewith a site can have a set of public/private pages and content
+ for **user** or **user group** - in this case it is a set of public/private pages only (without content and members)
+ **separately** - in this case a site can have a set of public/private pages, content and members of a site (users/organizations/user groups)

The site can be created as blank one (**Add → Blank Site**) or via **Site Template**.

**Site Template** consists of **Page Templates**.

We will create a page template: **Page Template → Add**. We will point a name as **CalendarPage**. Then go **Action → Edit** for created page template and select **Open Page Template**:

<p class="article-img2"></p>
![Liferay](images/02-28-page-template.png)

We will add a portlet to the **Calendar** page and select **1-coloumn** layout, as a result we will get:

<p class="article-img3"></p>
![Liferay](images/02-29-page-temp1.png)

After that we will create **SiteTemplate** (**Site Templates → Add**). We will assign a name as **CalendarSite**, we will get as a result:

<p class="article-img2"></p>
![Liferay](images/02-30-site-template.png)

We will add a new page to the section **Pages**, selecting herewith the created earlier **Page Template**:

<p class="article-img3"></p>
![Liferay](images/02-31-add-page.png)

Then we will create a new layout, using created **Site Template**: **Sites → Add → Calendar Site**.

The configuration of pages/portlets, that is specified in **Site Temaplate** will be applied to the created site.

<p class="article-img2"></p>
![Liferay](images/02-32-site.png)

When creating a site template you can specify the pages (**public** or **private**), to which the template will be applied, and whether the template changes will change automatically the created site.

<p>&nbsp;</p>
To add the site members it is necessary to select **Site Membership** in the **Users** section, then select **Users**, click on **Assign Users**,

<p class="article-img2"></p>
![Liferay](images/02-33-site-members.png)

then select the users you need, click on **Save**:

<p class="article-img2"></p>
![Liferay](images/02-34-site-userss1.png)

Similarly, the organization and user groups can be specified as site members.

<p>&nbsp;</p>
### LIFERAY PORTAL structure

<p class="article-img2"></p>
![Liferay](images/02-35-Liferay.jpeg)

<p>&nbsp;</p>
### LIFERAY configuration

The facilities for configuration / administration Liferay  are located on the **'Configuration'** tab in the **Control Panel**:

<p class="article-img2"></p>
![Liferay](images/02-36-config.png)

