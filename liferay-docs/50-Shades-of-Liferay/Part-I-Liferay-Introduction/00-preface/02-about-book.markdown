# About Book [](id=about-book)

This book is recommended for Java-developers, which want to start development with Liferay,
and also for web-developers and portal administrators.

<p>&nbsp;</p>
### Required skills:

- Java (Core, EE)

- Database - MySQL

- Application server - Apache Tomcat

- Build tools - Ant/Maven

- Frontend (HTML/CSS/JS)

- IDE - Intellij IDEA

- OS - Windows7/Ubuntu

Although those skills are not required, they're desirable. The mentioned above tools will be used in book as examples.

<p>&nbsp;</p>
### Roadmap

First chapter of book explains how to download, install and configure Liferay. It also explains how to configure development 
environment in Intellij IDEA IDE.

Main concepts of Liferay portal are covered in the second chapter. This is theoretical chapter, explaining 
how Liferay works without deeping inside development process. But this information is required to know
before you start development with Liferay.

Chapters 3-6 are practical ones, they have instructions how to create your first portlet, theme, layout and hook.

Chapters 7-9 are dedicated to themes development.


