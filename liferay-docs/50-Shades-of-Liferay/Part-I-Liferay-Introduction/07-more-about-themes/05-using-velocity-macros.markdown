# Using VELOCITY-macros [](id=using-velocity-macros)

Now we consider the use of [velocity-macros](http://velocity.apache.org/engine/releases/velocity-1.7/user-guide.html#Velocimacros) in the template of a theme.

<p>&nbsp;</p>
### What is the VELOCITY-macro?

**Velocity-macro** admits the creation of the iterative fragment of the template, that then can be  reused. Also, it can take one or more parameters that can be used in this fragment of the template. 

Macro in *Velocity* is created by using the command:

**#macro**(macros_name param1 ... paramN)

MACROS_BODY

**#end**

Then it can be called as:

**#macros_name**(arg1 ... argN)

The parameters (and, accordingly, the arguments) can be absent.

<p>&nbsp;</p>
### How velocity-macros are used in Liferay?

During the development the template of the theme we have already used the macros that was created in *Liferay* by default, for example:

1) *dockbar*:

<p class="article-img2"></p>
![Liferay](images/07-23-macro2.png)

2) *breadcrumb*:

<p class="article-img2"></p>
![Liferay](images/07-23-macro1.png)

These macros have been declared in the file /ROOT/WEB-INF/lib/portal-impl.jar!/VM_liferay.vm:

<p class="article-img2"></p>
![Liferay](images/07-24-liferay-macro.png)

and can be used during the theme development.

<p>&nbsp;</p>
### Development of the own velocity-macro

Earlier in this chapter we added to this theme *Language* portlet. Now we do it with the help of the velocity-macro.

We take out the code for displaying *Language* portlet into separate macro (call it *'language_portlet'*) in *init_custom.vm*:

<p class="article-img2"></p>
![Liferay](images/07-25-lan-macro.png)

Now we can use the created macro in the template of the theme:

<p class="article-img2"></p>
![Liferay](images/07-26-lan-portlet.png)

As we can see, after creating macro for *Language* portlet, we can use it just as well as *#dockbar ()* or *#breadcrumbs ()*. It reduces code of theme template and admits reuse it.

<p>&nbsp;</p>
### More elaborate Velocity-macro

Now we develop more elaborate Velocity-macro.

Earlier in this chapter we have changed the navigation menu in the theme so that it  displays the pages of second level. We did this by using a nested cycle. But suppose that it will need to display the pages of the third, fourth level, etc. Then we have to write a nested cycle for each level of nesting.

Not to do so, it is possible to use a [recursion](https://ru.wikipedia.org/wiki/%D0%A0%D0%B5%D0%BA%D1%83%D1%80%D1%81%D0%B8%D1%8F). Similarly to the [recursion on **jsp**](http://vkoshelenko.blogspot.com/2013/11/recursion-on-jsp.html), it is possible to develop a velocity-macro, that will take the current page as a parameter, display the current level of the drop-down menu, and call itself for each of the subpages. Thus, the macro will call itself and display the next level of the menu as long as the subpages will be there.

Add the macro for the navigation in *init_custom.vm*:

<p class="article-img2"></p>
![Liferay](images/07-27-nav_macro.png)

As we can see, the macro takes the parameter *$nav_elem* and calls itself for each of *$nav_elem_child*.

Now we use this macro in the navigation:

<p class="article-img2"></p>
![Liferay](images/07-28-nav_menu.png)

Also, we fix up the css-styles for navigation:

<p class="article-img2"></p>
![Liferay](images/07-29-nav-css.png)

Add subpages of the several levels of nesting:

<p class="article-img2"></p>
![Liferay](images/07-30-pages.png)

And look at the result:

<p class="article-img2"></p>
![Liferay](images/07-31-menu.png)

As we can see, with the help of the developed macro the submenu is displayed for any level of nesting pages.


