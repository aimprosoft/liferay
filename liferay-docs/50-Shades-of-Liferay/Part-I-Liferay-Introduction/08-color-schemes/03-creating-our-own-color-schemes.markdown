# Creating our own COLOR SCHEMES [](id=creating-our-own-color-schemes)

For created theme **'aimprosoft-theme'** we will create two color schemes: **orange** and **blue**.

We create the file *liferay-look-and-feel.xml* (on basis of **classic** scheme):

<p class="article-img2"></p>
![Liferay](images/08-03-laf.png)

Here we described two color schemes for **aimprosoft-theme**, indicating for each of them css-class and basic path to images.

In the folder *aimprosoft-theme/src/main/webapp/images/color_schemes* we will create the folders for each of color schemes. We will deposit the file **thumbnail.png** in each of these folders.

<p class="article-img2"></p>
![Liferay](images/08-04-col-themes.png)

After recompiling of the theme we will see that the two color schemes became available for **"aimprosoft-theme"**:

<p class="article-img3"></p>
![Liferay](images/08-05-aim-col-themes.png)

Now we specify the styles for color schemes.

We create the files **css/color_schemes/blue.css** and **css/color_schemes/orange.css**:

<p class="article-img2"></p>
![Liferay](images/08-06-col-sch-css.png)

and specify various colors of background:

<p class="article-img2"></p>
![Liferay](images/08-07-blue-col.png)

<p class="article-img2"></p>
![Liferay](images/08-07-or-col.png)

Similarly we can specify another styles inside of classes **.blue** and **.orange**.

We apply the created color schemes and check the result.

1. *Blue Color Scheme*

<p class="article-img2"></p>
![Liferay](images/08-08-col-sch-blue.png)

2. *Orange Color Scheme*

<p class="article-img2"></p>
![Liferay](images/08-09-col-sch-or.png)


