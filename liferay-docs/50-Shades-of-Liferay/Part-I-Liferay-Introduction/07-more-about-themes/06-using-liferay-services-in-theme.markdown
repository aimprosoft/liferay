# Using Liferay-services in theme [](id=using-liferay-services-in-theme)

Now we consider the use of Liferay-services in a theme.

<p>&nbsp;</p>
### What is Liferay-services?

**Liferay-services** – classes that allow to work with liferay models (create, retrieve from the database, save, etc.). For each **[Entity] model** in Liferay its own **[Entity] Service** was created.

[Liferay-services](http://www.liferay.com/community/wiki/-/wiki/Main/Access+Liferay+Services+in+Velocity) can be used in your theme by using the service locator:

**#set($myService = $serviceLocator.findService("[Entity]Service"))**

After obtaining the service by such way, it is possible to call its methods for receiving the required values in the theme. 

For example, we can obtain the service for users and for the layout in the following manner:

<p class="article-img2"></p>
![Liferay](images/07-32-ser1.png)

In Liferay the services can be **local** and **remote**.

**Local services:**

+ Classes **com.liferay.portal.service.[Entity]LocalService**
+ Do not contain the checking of permissions before calling

**Remote services:**

+ Classes **com.liferay.portal.service.[Entity]Service**
+ Before the call, they check permissions , and put the **PrincipalException** if the user does not have enough permissions for method calling

It is better to use **local services** for avoiding the appearance of **PrincipalException**.

<p>&nbsp;</p>
### Example of Liferay-services

We will do it so that the page **'/administration'** will be displayed in the navigation only for users of **'Aim ProSoft'** organization, which have there the role **'AimproSoft Admin'**.

To do this, we will connect the necessary services in **init_custom.vm**:

<p class="article-img2"></p>
![Liferay](images/07-33-ser2.png)

Using these services, we will define, whether the current user has the role **"AimProSoft Admin"** in the **"AimProSoft"** organization.

<p class="article-img2"></p>
![Liferay](images/07-34-ser3.png)

In **navigation.vm** for checking we use the variable **$isAimAdmin** that was declared in **init_custom.vm**.

<p class="article-img2"></p>
![Liferay](images/07-35-ser4.png)

Here the checking **isAimAdmin** for the page **'/administration'** is executed. That is, only users with the role **'Aim ProSoft Admin'** will be able to see this page.

After deployment we see that **'AimProSoft Admin'** can see the page **'Administration'**,

<p class="article-img2"></p>
![Liferay](images/07-36-ser-role-Y.png)

but a user without this role - can not.

<p class="article-img2"></p>
![Liferay](images/07-37-ser-role-N.png)

(**P.S.** The same functional can be realized via display of the permissions on the page; here it was so done for demonstration of capabilities of using the services in the theme).


