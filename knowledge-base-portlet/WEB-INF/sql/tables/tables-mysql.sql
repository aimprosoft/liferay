create table KBArticle (
	uuid_ varchar(75) null,
	kbArticleId bigint not null primary key,
	resourcePrimKey bigint,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	rootResourcePrimKey bigint,
	parentResourceClassNameId bigint,
	parentResourcePrimKey bigint,
	kbFolderId bigint,
	version integer,
	title longtext null,
	urlTitle varchar(75) null,
	content longtext null,
	description longtext null,
	priority double,
	sections longtext null,
	viewCount integer,
	latest tinyint,
	main tinyint,
	sourceURL longtext null,
	status integer,
	statusByUserId bigint,
	statusByUserName varchar(75) null,
	statusDate datetime null
) engine InnoDB;

create table KBComment (
	uuid_ varchar(75) null,
	kbCommentId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	classNameId bigint,
	classPK bigint,
	content longtext null,
	userRating integer,
	status integer
) engine InnoDB;

create table KBFolder (
	uuid_ varchar(75) null,
	kbFolderId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	parentKBFolderId bigint,
	name varchar(75) null,
	urlTitle varchar(75) null,
	description longtext null
) engine InnoDB;

create table KBTemplate (
	uuid_ varchar(75) null,
	kbTemplateId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	title longtext null,
	content longtext null
) engine InnoDB;
