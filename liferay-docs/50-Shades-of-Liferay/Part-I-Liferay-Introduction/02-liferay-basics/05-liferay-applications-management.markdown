# Liferay Applications Management [](id=liferay-applications-management)

By default Liferay contains a large number of applications (portlets) for different purposes (for example, **Wiki**, **Blog**, **Message Boards**, **WebContent**, etc.).

To add a portlet to the page you need select **Applications** from the **MenuBar**, and then select the appropriate portlet. After clicking **Add** the portlet will be embedded to the page.

Portlets can be **instanceable** (can be added to a page several times) and **not-instanceable** (can be added only once). Each portlet has its own unique **ID**.

For instanceable portlets for providing their uniqueness, **portlet namespace** is written additionally to **ID** (for each portlet on the page the unique **portlet namespace** is generated).

After adding a portlet to a page, you can assign it the configuration settings and right of the access (**permissions**). Here, for example, the form for specifying permissions for **Calendar** portlet.

<p class="article-img2"></p>
![Liferay](images/02-37-perms.png)

To display/hide this portlet for certain role it is necessary to check/uncheck the flag **View** (opposite the role).

The visual settings for portlet displaying it is possible to specify in **'Look and Feel'** menu.

You can specify a custom title for the portlet, display/hide the borders, prescribe the custom **css** rules for portlet:

<p class="article-img2"></p>
![Liferay](images/02-38-lookAndFeel.png)

The portlets on a page can be moved by drag-and-drop (according to the selected layout).

The list of all installed applications can be viewed in **Control Panel -> Apps**

<p class="article-img2"></p>
![Liferay](images/02-39-apps.png)

Here you can activate/deactivate or remove the desired application.

On the **Install** tab, you can set the desired application (via **upload war/lpkg** file or via **URL**).

To install an application from the [Liferay MarketPlace](https://www.liferay.com/marketplace) is necessary to go to **Store** on the **Apps** tab and log in (if you don't have an account - register on [liferay.com](www.liferay.com) before).

After selecting the desired application click on **Purchase**. After that it should be displayed on the tab **'Purchased'**:

<p class="article-img2"></p>
![Liferay](images/02-40-purchased.png)

Click on **Install**, and the application will be installed.

Developing own portlets is discussed in this book later.

