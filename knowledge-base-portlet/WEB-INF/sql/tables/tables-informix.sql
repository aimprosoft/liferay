create table KBArticle (
	uuid_ varchar(75),
	kbArticleId int8 not null primary key,
	resourcePrimKey int8,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	rootResourcePrimKey int8,
	parentResourceClassNameId int8,
	parentResourcePrimKey int8,
	kbFolderId int8,
	version int,
	title lvarchar,
	urlTitle varchar(75),
	content text,
	description lvarchar,
	priority float,
	sections lvarchar,
	viewCount int,
	latest boolean,
	main boolean,
	sourceURL lvarchar,
	status int,
	statusByUserId int8,
	statusByUserName varchar(75),
	statusDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table KBComment (
	uuid_ varchar(75),
	kbCommentId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	content lvarchar,
	userRating int,
	status int
)
extent size 16 next size 16
lock mode row;

create table KBFolder (
	uuid_ varchar(75),
	kbFolderId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentKBFolderId int8,
	name varchar(75),
	urlTitle varchar(75),
	description lvarchar
)
extent size 16 next size 16
lock mode row;

create table KBTemplate (
	uuid_ varchar(75),
	kbTemplateId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	title lvarchar,
	content text
)
extent size 16 next size 16
lock mode row;
