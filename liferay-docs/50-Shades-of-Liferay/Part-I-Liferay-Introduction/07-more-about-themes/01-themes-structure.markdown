# Themes structure [](id=themes-structure)

After creating the theme we have the structure:

<p class="article-img3"></p>
![Liferay](images/07-01-theme-structure.png)

**/css** - css-files of the theme

**/images** - images, that are used in the theme

**/js** - js-files of the theme

**/templates** - folder with templates for theme consists of such files:

+ **init_custom.vm** - in this file custom velocity-variables are created, which then can be used in the theme template. Variables are specified via command **#set ($name = value)**. Usually a name of a variable begins with the sign **'$'**. In the classic theme we see how the value of the variable **$css_class** is changing: 

<p class="article-img3"></p>
![Liferay](images/07-02-init-vm.png)

List of all predefined variables is located in the file **ROOT/html/themes/_unstyled/templates/init.vm**:

<p class="article-img3"></p>
![Liferay](images/07-03-init-file-str.png)

In this file we see the following:

<p class="article-img2"></p>
![Liferay](images/07-04-init-file-cont.png)

All variables which are asserted in this file, you can use in your themes.

+ **navigation.vm** - in this file is defined the template for menu of the navigation in the theme. In the classic theme has already been created the navigation for pages and subpages of first level.

<p class="article-img2"></p>
![Liferay](images/07-05-nav.png)

It is possible to use this file as a basic for navigation development in our own themes.

+ **portal_normal.vm** - in this file is created a template of the theme. If we open a template for classic theme, we will see that it has the following structure:

<p class="article-img2"></p>
![Liferay](images/07-06-portal-normal.png)

Dockbar is displayed above, then *wrapper* follows, consisting of header, content and footer.

The logotype, site name and navigation are placed in the header (only for logged users).

Breadcrumb and the portlets are displayed in the content.

The standard caption **'Powered By Liferay'** is displayed in the footer.

+ **portal_pop_up.vm** - template for pop-up

+ **portlet.vm** - template for portlet has such structure

<p class="article-img2"></p>
![Liferay](images/07-07-portlet.png)

Portlets are displayed on the portal page according to this template.

**/WEB-INF** - folder with the configuration files

+ **liferay-plugin-package.properties** - description properties of the theme

+ **web.xml** - deployment descriptor


