# Creating new module in IDEA [](id=creating-new-module-in-IDEA)

Create a new module *'hello-world'* by using of *Maven archetype*.

To do it, go *File → New Module*..., select *Maven*, check *'Create from archetype'* and select *'liferay-portlet-archetype'*:

<p class="article-img3"></p>
![Liferay](images/03-01-maven-archtype.png)

Click on **Next**, specify *groupld* and *artifactld* 

<p class="article-img3"></p>
![Liferay](images/03-02-new-module.png)

Click on **Next**, setup *Maven* (if it was not setup formerly):

<p class="article-img3"></p>
![Liferay](images/03-03-new-module-2.png)

Click on **Next**, specify the way to the module and click on **Finish**.

Maven will create the structure of the project automatically.

After that, add Maven profile. To do it, create the file ***settings.xml*** in the folder ***$HOME$/.m2***:

<p class="article-img3"></p>
![Liferay](images/03-04-settings-maven.png)

Select this created profile on the tab Maven in IDEA. After that Maven will pull up the necessary dependencies.


