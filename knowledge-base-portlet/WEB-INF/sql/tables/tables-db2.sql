create table KBArticle (
	uuid_ varchar(75),
	kbArticleId bigint not null primary key,
	resourcePrimKey bigint,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75),
	createDate timestamp,
	modifiedDate timestamp,
	rootResourcePrimKey bigint,
	parentResourceClassNameId bigint,
	parentResourcePrimKey bigint,
	kbFolderId bigint,
	version integer,
	title varchar(750),
	urlTitle varchar(75),
	content clob,
	description varchar(750),
	priority double,
	sections varchar(750),
	viewCount integer,
	latest smallint,
	main smallint,
	sourceURL varchar(750),
	status integer,
	statusByUserId bigint,
	statusByUserName varchar(75),
	statusDate timestamp
);

create table KBComment (
	uuid_ varchar(75),
	kbCommentId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75),
	createDate timestamp,
	modifiedDate timestamp,
	classNameId bigint,
	classPK bigint,
	content varchar(750),
	userRating integer,
	status integer
);

create table KBFolder (
	uuid_ varchar(75),
	kbFolderId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75),
	createDate timestamp,
	modifiedDate timestamp,
	parentKBFolderId bigint,
	name varchar(75),
	urlTitle varchar(75),
	description varchar(750)
);

create table KBTemplate (
	uuid_ varchar(75),
	kbTemplateId bigint not null primary key,
	groupId bigint,
	companyId bigint,
	userId bigint,
	userName varchar(75),
	createDate timestamp,
	modifiedDate timestamp,
	title varchar(750),
	content clob
);
