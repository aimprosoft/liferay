# What is Liferay? [](id=what-is-liferay)

**Liferay** is a platform written on Java, intended for web-development, and which provides a lot of ready-for-use solutions for sites creation.
It is a web-portal with capabilities for users management, roles and permissions management, and also pages/content, applications management.

Liferay has a lot of built-in applications (portlets) - such as, **Blogs**, **Message Boards**, **Wiki**, **Calendar**, etc.
In most case Liferay development is development of separate applications (**portlets**, **themes**, **layouts**) and their composition into a single site (portal).
In some cases (when you need to modify/extend Liferay's default behavior) **Liferay-hooks** are developed.

All these questions are described in more details later in this book.


