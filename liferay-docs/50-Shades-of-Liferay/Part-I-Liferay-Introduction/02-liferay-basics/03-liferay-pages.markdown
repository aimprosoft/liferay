# Liferay Pages [](id=liferay-pages)

We have just reviewed Liferay's page structure. Now we'll discuss Liferay pages in more details.

<p>&nbsp;</p>
### What is Liferay Page?

**Liferay page** (also called **'layout'**) is a separate portal page, which has it's own URL, set of portlets on it, theme, layout, permissions settings, etc.
Each page in Liferay belongs to some site, has it's owner, permissions set, and displays information rendered by portlets on this page.

<p>&nbsp;</p>
### Page Management

To create a new Liferay page, you need to either use **MenuBar** for this, or to do it from inside **Site Pages** menu (**'Admin' -> 'Site Administration' -> 'Pages'** from **Dockbar**).

Here is example of adding page from **MenuBar**:

<p class="article-img3"></p>
![Liferay](images/02-02-add-page.jpg)

Click on **+** icon (1), **Page** tab (2), specify page name (3), choose layout for page (4), and press **Add Page** (5) button. New page will be created.

For advanced page management go to **Admin -> Site Administration -> Pages** from **Dockbar**.
You'll see the following:

<p class="article-img3"></p>
![Liferay](images/02-03-site-pages.jpg)

Here you can perform page management. You can do the following:

- change page order using drag-and-drop
- add new pages
- add child pages for current page
- delete page (to delete some page you need to be currently on some other page)
- modify permissions on page
- specify page name and URL for a page
- make page hidden to hide it from navigation menu
- change page type and layout
- specify custom settings for a page (custom fields, SEO settings, etc.)

For example, to add child page for some already existing page - select this page in the pages list at the left,
click **Add Child Page** button (1), specify page name (2), and click **Add Page**:

<p class="article-img3"></p>
![Liferay](images/02-04-add-child-page.jpg)

If you go back to site home page, you'll see that subpage is displayed in the navigation menu:

<p class="article-img3"></p>
![Liferay](images/02-05-sub-page.jpg)

If you edit this subpage, and mark **Hide** from **Navigation Menu** flag, you'll see, that page will no longer be displayed in the navigation.

<p>&nbsp;</p>
### Page Types

Liferay has different types of pages. The default one is **Layout** - it's standard, empty by default page, which is displayed in navigation menu.
Portlets may be added to such page, themes and layouts may be also applied to this type of page. In most cases this page type is used.

But there are different page types, here are they:

- **Link to a Page of This Site** - as the name says, this is a link to some page within site. Page of this type doesn't have it's own content,
it's used to redirect to some other page within the same site by clicking on this page in navigation menu

- **Link to URL** - this is similar to previous type, but may refer to a page of some other site within portal, or even to some external URL

- **Panel** - this type of page is used to work with different portlets on one and the same page. When you set page type to **Panel** in **Site Pages** menu, 
you can specify which applications (portlets) will be available on this panel page:

<p class="article-img3"></p>
![Liferay](images/02-06-panel-page.jpg)

When you go to panel page, you'll see, that it contains two parts: list of available applications in the left section, and selected application in the right one:

<p class="article-img3"></p>
![Liferay](images/02-07-panel-page-display.jpg)

This type of page may be used, if you need to work with different portlets. In this case you don't need to create pages and add portlets you need to those pages - all the portlet you need are already on panel page.
Panel page type doesn't allow to select page layout and to add applications to a page.

- **Embedded** - it's a Liferay page, which contains **IFrame**, which displays content from specified URL. 
When you select *Embedded* page type, you may specify URL to display:

<p class="article-img3"></p>
![Liferay](images/02-08-embedded.png)

When you go to embeded page, you'll see that this URL is displayed withing **IFrame** on this page:

<p class="article-img3"></p>
![Liferay](images/02-09-embedded-display.png)

Adding portlets to embedded page and changing it's layout is also not allowed (as with panel pages).

<p>&nbsp;</p>
### Public and Private pages

In **Site Pages** menu we see, that there are **Public Pages** and **Private Pages**:

<p class="article-img3"></p>
![Liferay](images/02-10-pages-private-public.jpg)

Here is the difference between them:

<p class="article-table"></p>
| **Public Pages**                                                                             |   **Private Pages**                                                                              |
|:-------------------------------------------------------------------------------------------- |:-------------------------------------------------------------------------------------------------|
| Accessible by all users by default (regardless of their membership in site)                  |   Accessible only by site members                                                                |
| Have the following URL structure: `http://{host}:{port}/web/{site-name}/{page-friendly-url}` |   Have the following URL structure: `http://{host}:{port}/group/{site-name}/{page-friendly-url}` |

<p>&nbsp;</p>
Page management for private pages is the same as for public pages (which was covered earlier).
If you create private page, you'll see, that it's **Friendly URL** contains **/group/** in it's URL instead of **/web/** (as it was for public pages):  

<p class="article-img3"></p>
![Liferay](images/02-11-private-page.jpg) 

<p>&nbsp;</p>
<p class="article-note"></p>
**NOTE:** 

Those private/public friendly URLs settings are configured in **portal.properties**:

`layout.friendly.url.private.group.servlet.mapping=/group`

`layout.friendly.url.public.servlet.mapping=/web`

and may be changed in **portal-setup-wizard.properties** file.

<p>&nbsp;</p>
If you try to access some private page being not logged in (for example, **/group/guest/**), 
you'll be redirected to the default public page in the same site (**/web/guest/** for the default **Liferay** site) with full-screened (in maximized mode) **Sign In**
portlet on it (1). Once you sign in, you'll be redirected back to the private page which you originally requested (it's URL is stored in **redirect** parameter (2) in the URL):

<p class="article-img2"></p>
![Liferay](images/02-13-private-redirect.jpg) 

but only in case when you're a member of the site and have sufficient permissions to view this page. Otherwise, if you can not see this page - **'Not Found'** exception will be shown to you:

<p class="article-img3"></p>
![Liferay](images/02-14-not-found.jpg)

(the same exception is shown if page really doesn't exist).

You can quickly navigate to public/private pages of your sites from the Dockbar menu:

<p class="article-img3"></p>
![Liferay](images/02-12-navigate-pages.jpg) 

<p>&nbsp;</p>
### User's pages 

Each user in Liferay can have his own personal site - a set of public pages (called **'My Profile'**) and set of private pages (called **'My Dashboard'**).

<p class="article-table"></p>
| **My Profile**                                                                                 |   **My Dashboard**                                                                                |
|:---------------------------------------------------------------------------------------------- |:------------------------------------------------------------------------------------------------  |
| Accessible by all users                                                                        |   Accessible only by owner (and portal administrator)                                             |
| Have the following URL structure: `http://{host}:{port}/web/{screen-name}/{page-friendly-url}` |   Have the following URL structure: `http://{host}:{port}/user/{screen-name}/{page-friendly-url}` |

<p>&nbsp;</p>
It's similar to site public/private pages, but here user's screen name is used in URL instead of site group friendly URL, and */user/* prefix is used for private pages (instead of */group/*).

**My Profile** (1) and **My Dashboard** (2) are accessible from **Dockbar**:

<p class="article-img3"></p>
![Liferay](images/02-15-profile-dashboard.jpg)

**My Profile** pages:

<p class="article-img3"></p>
![Liferay](images/02-16-profile.jpg)

**My Dashboard** pages:

<p class="article-img3"></p>
![Liferay](images/02-17-dashboard.jpg)

As with site pages, page management for **My Profile** and **My Dashboard** is available from *'Admin' -> 'Site Administration' -> 'Pages'* menu in **Dockbar**

<p class="article-img3"></p>
![Liferay](images/02-18-manage-profile.jpg)

Here you can manage pages for **My Profile** (1) and **My Dashboard** (2) in the same way, as you do this for site public/private pages.

When you create new user - both public (profile) and private (dashboard) pages are created for him automatically in the default Liferay configuration.
To disable automatic creation of public/private pages for user, the following properties may be used in **'portal-setup-wizard.properties'**:

`layout.user.public.layouts.auto.create=false`

`layout.user.private.layouts.auto.create=false`

To disable public/private pages at all, you may use:

`layout.user.public.layouts.enabled=false`
    
`layout.user.private.layouts.enabled=false`
    
You may also specify your custom structure of created public/private page for user by overwriting those properties:

<p class="article-img3"></p>
![Liferay](images/02-19-layout-props.jpg)

If you need to create more than one page for user - you may also specify which LAR to use:

`default.user.private.layouts.lar=${liferay.home}/{path-to-lar-file}/{lar-file}.lar`

<p>&nbsp;</p>
### Export/Import pages

Liferay provides capability to import/export site pages with a help of **LAR files (Layout ARchives)**.
If you need to copy your site pages to some other Liferay instance - you may create **.LAR** file by exporting pages from your site,
and then import this file to target Liferay instance.

To create **LAR** file, you need to press **Export** button from inside **Site Pages** (if you need to export private pages - click on **Private Pages** tab before):

<p class="article-img2"></p>
![Liferay](images/02-20-export.jpg)

**Export** popup will be opened, where you can specify export settings - name of exported **LAR** file, choose which pages and which content to export.

Here I specify which pages to include into the **LAR** file being created:

<p class="article-img2"></p>
![Liferay](images/02-21-lar-pages.jpg)

(this feature will appeared in **6.2** only)

After **LAR** has been created, you may download it:

<p class="article-img2"></p>
![Liferay](images/02-22-lar-download.jpg)

To import **LAR** file, click on **Import** button (assuming, you're already inside **Site Pages** menu for the site you need):

<p class="article-img2"></p>
![Liferay](images/02-23-import.jpg)

Upload **LAR** file in the opened **Import** popup:

<p class="article-img2"></p>
![Liferay](images/02-24-import2.jpg)

Click on **Continue**, select import options, click on **Import**:

<p class="article-img2"></p>
![Liferay](images/02-25-import3.jpg)

Make sure, that import process has finished successfully:

<p class="article-img2"></p>
![Liferay](images/02-26-import-success.jpg)

After this all pages and content from the **LAR** file should be available in the site where you uploaded this **LAR** file.

<p>&nbsp;</p>
### Layout Templates

**Layout Templates (layouts)** define the displaying of the portlets on the page. By default Liferay contains ten layouts.

To change the page layout we should click on **Edit** in the **MenuBar**, select the desired layout and click on **Save**.

<p class="article-img3"></p>
![Liferay](images/02-41-layout.png)

After applying the layout to the page the portlets can be moved in it according to the structure of the selected layout.

In most cases it is enough for developing the layouts, that are provided by default. But in some cases the portlets should be placed in certain layout, but the acceptable layout is hasn't included in the set. To do this, you can write a custom layout (or download the ready from [MarketPlace](https://www.liferay.com/marketplace)).

Sometimes, instead of a custom layout we can use the portlet **Nested portlets**.

More about the **Layout Templates** read in this book later.

<p>&nbsp;</p>
### Liferay Themes

**Liferay Themes (themes)** - are liferay applications, that allow us to define the structure and look-and-feel for Liferey pages.

If layouts define only the structure for portlet layout, then the themes allow us to determine the structure of the entire portal page (including dockbar, header, content, footer, etc.), as well as css-rules for it.

By default, there are two themes  in Liferay (**Welcome** and **Classic**).

You can change the theme for a page through the **MenuBar** (**Edit → Look and Feel**)

<p class="article-img3"></p>
![Liferay](images/02-42-theme.png)

In the menu **Site Pages** we can specify the theme as for the entire site (for all **private** or all **public** pages), as well for specific pages.

More about the **Liferay Themes** development read in this book later.

