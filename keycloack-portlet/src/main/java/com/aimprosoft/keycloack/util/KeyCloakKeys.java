package com.aimprosoft.keycloack.util;

public interface KeyCloakKeys {

    String REALM_NAME           = "keycloak.realm.name";
    
    String CLIENT_ID            = "keycloak.client.id";

    String CLIENT_SECRET        = "keycloak.secret";

    String BASE_URL             = "keycloak.base.url";
    
    String REDIRECT_URL         = "keycloak.redirect.url";

    String DEFAULT_PASS         = "keycloak.default.password";

}
