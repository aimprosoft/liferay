package com.aimprosoft.keycloack.invoker.impl;

import com.aimprosoft.keycloack.invoker.KeyCloackInvoker;
import com.aimprosoft.keycloack.util.KeyCloackUtils;
import com.aimprosoft.keycloack.util.KeyCloakConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.List;

public class TokenInvoker extends KeyCloackInvoker {

    private String code;

    public TokenInvoker(long companyId, String code) {
        super(companyId);
        this.code = code;
    }

    public TokenInvoker(long companyId, String redirectUrl, String code) {
        super(companyId, redirectUrl);
        this.code = code;
    }

    @Override
    public String buildInvokeUrl() {
        
        return KeyCloackUtils.getTokenUrl(getCompanyId());
    }

    @Override
    public void fillFormParams(List<NameValuePair> formParams) {

        String clientId = KeyCloackUtils.getClientId(getCompanyId());
        String clientSecret = KeyCloackUtils.getClientSecret(getCompanyId());
        
        formParams.add(new BasicNameValuePair(KeyCloakConstants.CODE, code));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.GRANT_TYPE, KeyCloakConstants.AUTHORIZATION_CODE));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.REDIRECT_URI, getRedirectUrl()));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.CLIENT_ID, clientId));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.CLIENT_SECRET, clientSecret));
    }

    public String fetchAccessCode() throws Exception {
        String responseString = invokeKeyCloackService();
        _log.info("responseString: " + responseString);
        JSONObject jsonObject = new JSONObject(responseString);
        return String.valueOf(jsonObject.get(KeyCloakConstants.ACCESS_TOKEN));
    }
    
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private static Log _log = LogFactoryUtil.getLog(TokenInvoker.class);
}
