create table KBArticle (
	uuid_ varchar(75),
	kbArticleId int64 not null primary key,
	resourcePrimKey int64,
	groupId int64,
	companyId int64,
	userId int64,
	userName varchar(75),
	createDate timestamp,
	modifiedDate timestamp,
	rootResourcePrimKey int64,
	parentResourceClassNameId int64,
	parentResourcePrimKey int64,
	kbFolderId int64,
	version integer,
	title varchar(4000),
	urlTitle varchar(75),
	content blob,
	description varchar(4000),
	priority double precision,
	sections varchar(4000),
	viewCount integer,
	latest smallint,
	main smallint,
	sourceURL varchar(4000),
	status integer,
	statusByUserId int64,
	statusByUserName varchar(75),
	statusDate timestamp
);

create table KBComment (
	uuid_ varchar(75),
	kbCommentId int64 not null primary key,
	groupId int64,
	companyId int64,
	userId int64,
	userName varchar(75),
	createDate timestamp,
	modifiedDate timestamp,
	classNameId int64,
	classPK int64,
	content varchar(4000),
	userRating integer,
	status integer
);

create table KBFolder (
	uuid_ varchar(75),
	kbFolderId int64 not null primary key,
	groupId int64,
	companyId int64,
	userId int64,
	userName varchar(75),
	createDate timestamp,
	modifiedDate timestamp,
	parentKBFolderId int64,
	name varchar(75),
	urlTitle varchar(75),
	description varchar(4000)
);

create table KBTemplate (
	uuid_ varchar(75),
	kbTemplateId int64 not null primary key,
	groupId int64,
	companyId int64,
	userId int64,
	userName varchar(75),
	createDate timestamp,
	modifiedDate timestamp,
	title varchar(4000),
	content blob
);
