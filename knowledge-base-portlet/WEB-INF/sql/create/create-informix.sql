database sysmaster;
drop database lportal;
create database lportal WITH LOG;

create procedure 'lportal'.isnull(test_string varchar)
returning boolean;
IF test_string IS NULL THEN
	RETURN 't';
ELSE
	RETURN 'f';
END IF
end procedure;


create table KBArticle (
	uuid_ varchar(75),
	kbArticleId int8 not null primary key,
	resourcePrimKey int8,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	rootResourcePrimKey int8,
	parentResourceClassNameId int8,
	parentResourcePrimKey int8,
	kbFolderId int8,
	version int,
	title lvarchar,
	urlTitle varchar(75),
	content text,
	description lvarchar,
	priority float,
	sections lvarchar,
	viewCount int,
	latest boolean,
	main boolean,
	sourceURL lvarchar,
	status int,
	statusByUserId int8,
	statusByUserName varchar(75),
	statusDate datetime YEAR TO FRACTION
)
extent size 16 next size 16
lock mode row;

create table KBComment (
	uuid_ varchar(75),
	kbCommentId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	classNameId int8,
	classPK int8,
	content lvarchar,
	userRating int,
	status int
)
extent size 16 next size 16
lock mode row;

create table KBFolder (
	uuid_ varchar(75),
	kbFolderId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	parentKBFolderId int8,
	name varchar(75),
	urlTitle varchar(75),
	description lvarchar
)
extent size 16 next size 16
lock mode row;

create table KBTemplate (
	uuid_ varchar(75),
	kbTemplateId int8 not null primary key,
	groupId int8,
	companyId int8,
	userId int8,
	userName varchar(75),
	createDate datetime YEAR TO FRACTION,
	modifiedDate datetime YEAR TO FRACTION,
	title lvarchar,
	content text
)
extent size 16 next size 16
lock mode row;


create index IX_571C019E on KBArticle (companyId, latest);
create index IX_5A381890 on KBArticle (companyId, main);
create index IX_FBC2D349 on KBArticle (companyId, status);
create index IX_CFB8C81F on KBArticle (groupId, kbFolderId, status);
create index IX_4A49CDD6 on KBArticle (groupId, kbFolderId, urlTitle);
create index IX_379FD6BC on KBArticle (groupId, kbFolderId, urlTitle, status);
create index IX_694EA2E0 on KBArticle (groupId, latest);
create index IX_97C62252 on KBArticle (groupId, main);
create index IX_B0FCBB47 on KBArticle (groupId, parentResourcePrimKey, latest);
create index IX_D91D2879 on KBArticle (groupId, parentResourcePrimKey, main);
create index IX_55A38CF2 on KBArticle (groupId, parentResourcePrimKey, status);
create index IX_DF5748B on KBArticle (groupId, status);
create index IX_EC0D0F42 on KBArticle (groupId, urlTitle);
create index IX_5370EC28 on KBArticle (groupId, urlTitle, status);
create index IX_86BA3247 on KBArticle (parentResourcePrimKey, latest);
create index IX_1DCC5F79 on KBArticle (parentResourcePrimKey, main);
create index IX_2B6103F2 on KBArticle (parentResourcePrimKey, status);
create index IX_11CD0F56 on KBArticle (resourcePrimKey);
create index IX_A5A54614 on KBArticle (resourcePrimKey, groupId);
create index IX_5FEF5F4F on KBArticle (resourcePrimKey, groupId, latest);
create index IX_8EF92E81 on KBArticle (resourcePrimKey, groupId, main);
create index IX_49630FA on KBArticle (resourcePrimKey, groupId, status);
create index IX_A9E2C691 on KBArticle (resourcePrimKey, latest);
create index IX_69C17E43 on KBArticle (resourcePrimKey, main);
create index IX_4E89983C on KBArticle (resourcePrimKey, status);
create unique index IX_AA304772 on KBArticle (resourcePrimKey, version);
create index IX_C23FA26F on KBArticle (uuid_);
create index IX_4E87D659 on KBArticle (uuid_, companyId);
create unique index IX_5C941F1B on KBArticle (uuid_, groupId);

create index IX_9FE4C2A3 on KBComment (classNameId, classPK);
create index IX_47D3AE89 on KBComment (classNameId, classPK, status);
create index IX_20A6BD9C on KBComment (groupId);
create index IX_E8D43932 on KBComment (groupId, classNameId);
create index IX_828BA082 on KBComment (groupId, status);
create index IX_FD56A55D on KBComment (userId, classNameId, classPK);
create index IX_8E470726 on KBComment (uuid_);
create index IX_6CB72942 on KBComment (uuid_, companyId);
create unique index IX_791D1844 on KBComment (uuid_, groupId);

create index IX_1F52AB5D on KBFolder (groupId, parentKBFolderId);
create index IX_3FA4415C on KBFolder (groupId, parentKBFolderId, name);
create index IX_729A89FA on KBFolder (groupId, parentKBFolderId, urlTitle);
create index IX_30B67029 on KBFolder (uuid_);
create index IX_32D1105F on KBFolder (uuid_, companyId);
create unique index IX_1FD022A1 on KBFolder (uuid_, groupId);

create index IX_83D9CC13 on KBTemplate (groupId);
create index IX_9909475D on KBTemplate (uuid_);
create index IX_853770AB on KBTemplate (uuid_, companyId);
create unique index IX_40AA25ED on KBTemplate (uuid_, groupId);


