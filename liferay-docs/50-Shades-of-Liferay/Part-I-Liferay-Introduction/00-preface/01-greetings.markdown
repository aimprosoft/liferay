# Greetings [](id=greetings)

<p class="profile-img"></p>
![Profile](images/00-02-profile.jpg)

<p>&nbsp;</p>
Hi! 

My name is Vitaliy Koshelenko, I'm a Liferay developer at AimProSoft company.

I have been working with Liferay for almost 5 years already, including portlets development, hooks, themes, 
layouts development, Liferay customization, migration from previous versions and integration with external services.

When I started working with Liferay, it seemed quite difficult and incomprehensible for me.
Even small tasks took too much time, especially those ones, which required modifications in Liferay's internal code.
Most of time was spend for searching required information - due to lack of Liferay-specific knowledge and experience in Liferay development.
Although there is official Liferay documentation (https://dev.liferay.com) as well as specifications of portlet technology (https://jcp.org/en/jsr/detail?id=286),
it's quite hard to find what you need. In most cases you just become drown inside endless Liferay forum threads, Wiki pages, 
tutorials instead of finding the solution you need.  

There is no either good practical guide for beginners with step-by-step instructions for creation first portlet, theme, etc., or theoretical tutorial explaining in a few words how Liferay actually works. 

So, I decided to write this book to help new developers quickly understand main concepts of Liferay structure, see how it works with simple examples and begin development with Liferay. 
I'll try to make it easy and understandable, but at the same time practical and useful.

Hope, my efforts will not be wasted, and this book will help you in understanding Liferay.

<p>&nbsp;</p>

It is my **contact** info:

<p>&nbsp;</p>
_Skype_: **vitaliy.koshelenko**

_E-mail_: v.koshelenko@aimprosoft.com

_Profile at Liferay community_: [https://www.liferay.com/web/vet.kosh/profile](https://www.liferay.com/web/vet.kosh/profile)

_Profile at Liferay JIRA_: [https://issues.liferay.com/secure/ViewProfile.jspa?name=v.koshelenko](https://issues.liferay.com/secure/ViewProfile.jspa?name=v.koshelenko)

<p>&nbsp;</p>

You may contact me, if you have some questions, suggestions, recommendations.


