# Embedding the portlets into the theme [](id=embedding-the-portlets-into-the-theme)

The portlets can be added to a page in a conventional manner, or can be embedded into a theme. In this case, the portlet will be displayed on the pages of such theme by default.

The portlet is embedded into the theme via the command:

*$theme.runtime("PORTLET_ID")*,

where *PORTLET_ID* – is **id** of the necessary portlet.

We add to our theme the *Language* portlet to be able to change the language (do it in *portal_normal.vm*):

<p class="article-img2"></p>
![Liferay](images/07-18-lang.png)

If we recompiling the theme, we will see that the *Language* portlet will be added to the header of the theme (but will be displayed with the title/borders and it is not beautifully). To fix this, remove the **portletSetupShowBorders** via **$velocityPortletPreferences**:

<p class="article-img2"></p>
![Liferay](images/07-19-show-borders.png)

Also assign the styles for this portlet:

<p class="article-img2"></p>
![Liferay](images/07-20-lang-css.png)

After redeploying of the theme we will see that *Language* portlet is displayed normally on the right in the header:

<p class="article-img2"></p>
![Liferay](images/07-21-lang-portlet.png)

Now on the pages of our theme we can easily change the language.

Similarly to this, you can add any other portlet. **ID** of required portlet can be defined in the interface *com.liferay.portal.util.PortletKeys*:

<p class="article-img2"></p>
![Liferay](images/07-22-portlet-keys.png)


