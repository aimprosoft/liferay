# Color schemes for CLASSIC theme [](id=color-schemes-for-classic-theme)

Color schemes for theme are specified in the file **liferay-look-and-feel.xml**. For classic theme we see the following:

<p class="article-img2"></p>
![Liferay](images/08-01-col-scheme1.png)

As we see, for theme **'classic'** were created three color schemes: **Default**, **Dark** and **Light**. When we go to *'Look and Feel'* and select *'Classic'* theme, we will see, that three color schemes are available for this theme:

<p class="article-img3"></p>
![Liferay](images/08-02-col-sch.png)

Applying the different color schemes for **classic** theme, we will get the different styles for theme.


