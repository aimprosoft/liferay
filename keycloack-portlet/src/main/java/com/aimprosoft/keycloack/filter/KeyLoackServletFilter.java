package com.aimprosoft.keycloack.filter;

import com.aimprosoft.keycloack.invoker.impl.TokenInvoker;
import com.aimprosoft.keycloack.invoker.impl.UserInfoInvoker;
import com.aimprosoft.keycloack.model.KeyCloakUser;
import com.aimprosoft.keycloack.util.KeyCloackUtils;
import com.aimprosoft.keycloack.util.KeyCloakConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class KeyLoackServletFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();

        long companyId = PortalUtil.getCompanyId(request);

        String keyCloackRedirect = KeyCloackUtils.getRedirectUrl(companyId);
        String liferayRedirect = request.getParameter("redirect");
        if (liferayRedirect != null && !"".equals(liferayRedirect.trim())) {
            keyCloackRedirect = PortalUtil.getPortalURL(request) + liferayRedirect;
            session.setAttribute("keyCloackRedirect", keyCloackRedirect);
        }

        if (session.getAttribute("keyCloackRedirect") != null) {
            keyCloackRedirect = String.valueOf(session.getAttribute("keyCloackRedirect"));
        }

        User user = null;
        try {
            user = PortalUtil.getUser(request);
        } catch (Exception ignored) {   
        }
        if (user != null) {
            //User already logged in
            _log.info("userId: " + user.getUserId());
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        String code = servletRequest.getParameter(KeyCloakConstants.CODE);
        _log.info("KeyLoackServletFilter, code: " + code);
        if (code == null) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        if ("true".equals(session.getAttribute("access_token_requested"))) {
            //access token already requested
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        String accessToken = null;
        try {
            TokenInvoker tokenInvoker = new TokenInvoker(companyId, keyCloackRedirect, code);
            accessToken = tokenInvoker.fetchAccessCode();
            session.setAttribute("access_token", accessToken);
            session.setAttribute("access_token_requested", "true");
        } catch (Exception e) {
            _log.error("Can not fetch accessToken: " + e.getMessage());
            e.printStackTrace();
        }

        _log.info("KeyLoackServletFilter, accessToken: " + accessToken);
        KeyCloakUser keyCloakUser = null;
        try {
            UserInfoInvoker userInfoInvoker = new UserInfoInvoker(companyId, keyCloackRedirect, accessToken);
            keyCloakUser = userInfoInvoker.fetchKeyCloakUser();
        } catch (Exception e) {
            _log.error("Can not fetch keyCloack user info: " + e.getMessage());
            e.printStackTrace();
        }

        if (keyCloakUser != null) {
            session.setAttribute("keyCloackUser", keyCloakUser);
            response.sendRedirect(keyCloackRedirect);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }

        _log.info("KeyLoackServletFilter end.");
    }

    @Override
    public void destroy() {
    }

    private static Log _log = LogFactoryUtil.getLog(KeyLoackServletFilter.class);
}
