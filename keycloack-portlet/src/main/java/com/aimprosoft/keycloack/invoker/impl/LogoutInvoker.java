package com.aimprosoft.keycloack.invoker.impl;

import com.aimprosoft.keycloack.invoker.KeyCloackInvoker;
import com.aimprosoft.keycloack.util.KeyCloackUtils;
import com.aimprosoft.keycloack.util.KeyCloakConstants;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.List;

public class LogoutInvoker extends KeyCloackInvoker {

    public LogoutInvoker(long companyId) {
        super(companyId);
    }

    public LogoutInvoker(long companyId, String redirectUrl) {
        super(companyId, redirectUrl);
    }

    @Override
    public String buildInvokeUrl() {

        return KeyCloackUtils.getLogoutUrl(getCompanyId());
    }

    @Override
    public void fillFormParams(List<NameValuePair> formParams) {

        String redirectUrl = KeyCloackUtils.getRedirectUrl(getCompanyId());
        String clientId = KeyCloackUtils.getClientId(getCompanyId());
        String clientSecret = KeyCloackUtils.getClientSecret(getCompanyId());

        formParams.add(new BasicNameValuePair(KeyCloakConstants.GRANT_TYPE, KeyCloakConstants.AUTHORIZATION_CODE));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.REDIRECT_URI, redirectUrl));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.CLIENT_ID, clientId));
        formParams.add(new BasicNameValuePair(KeyCloakConstants.CLIENT_SECRET, clientSecret));
    }

}
