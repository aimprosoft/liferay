package com.aimprosoft.keycloack.invoker;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class KeyCloackInvoker {

    private long companyId;
    private String redirectUrl;

    public KeyCloackInvoker(long companyId) {
        this.companyId = companyId;
    }

    public KeyCloackInvoker(long companyId, String redirectUrl) {
        this.companyId = companyId;
        this.redirectUrl = redirectUrl;
    }

    public String invokeKeyCloackService(){
        
        String response = StringPool.BLANK;

        try {

            String invokeUrl = buildInvokeUrl();
            HttpPost httpPost = new HttpPost(invokeUrl);

            List<NameValuePair> formParams = new ArrayList<NameValuePair>();
            fillFormParams(formParams);

            httpPost.setEntity(new UrlEncodedFormEntity(formParams, "UTF-8"));
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=UTF-8");

            HttpClient httpClient = new DefaultHttpClient();

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            
            if (httpEntity != null) {

                response = EntityUtils.toString(httpEntity);
                _log.info("Response: " + response);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }
    
    
    public abstract String buildInvokeUrl();
    
    public abstract void fillFormParams(List<NameValuePair> nameValuePair);


    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    private static Log _log = LogFactoryUtil.getLog(KeyCloackInvoker.class);
}
