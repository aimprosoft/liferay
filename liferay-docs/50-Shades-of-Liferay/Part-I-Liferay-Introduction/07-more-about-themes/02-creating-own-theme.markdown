# Creating own theme [](id=creating-own-theme)

After getting acquainted with theme structure, we create own theme **'aimprosoft-theme'** on basis of classic theme:

1) Change standard inscription **'Powered By Liferay'** to **'Powered By AimProSoft'** in *portal_normal.vm*

<p class="article-img2"></p>
![Liferay](images/07-08-pow-by.png)

2) Replace *favicon.png* and *thumbnail.png* files on logotype of our theme:

<p class="article-img3"></p>
![Liferay](images/07-09-img.png)

3) Change the color of the background. To do it, add the variable *$bodyBackground: #f19a1a;* in the file *css/_aui_variables.css*:

<p class="article-img3"></p>
![Liferay](images/07-10-aui-var.png)

4) We do so that the inscription **'Developed by AimProSoft'** will be displayed in each portlet. To do that, we change the file *templates/portlet.vm*:

<p class="article-img2"></p>
![Liferay](images/07-11-portlet-mod.png)

5) Assign our own styles for the theme.

Add the new file *aimprosoft.css*:

<p class="article-img3"></p>
![Liferay](images/07-12-aim-css.png)

and register it in *main.css*:

<p class="article-img3"></p>
![Liferay](images/07-13-main-css.png)

Deploy the theme and look what is the result:

<p class="article-img1"></p>
![Liferay](images/07-14-theme.png)


