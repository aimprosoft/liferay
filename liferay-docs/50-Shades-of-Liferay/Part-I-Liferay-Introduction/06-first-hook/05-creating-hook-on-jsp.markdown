# Creating hook on JSP [](id=creating-hook-on-JSP)

One of the main technology that is used in Liferay, is Struts.

Therefore, it is possible to define which *jsp* in which portlet is used via *struts action* (file /ROOT/WEB-INF/struts-config.xml).

To do it, go on *Login* portlet:

<p class="article-img3"></p>
![Liferay](images/06-04-login-portlet.png)

In *liferay-hook.xml* we register *custom-jsp-dir*:

<p class="article-img2"></p>
![Liferay](images/06-08-custom-jsp.png)

We create the folder *custom* inside of *webapp*, and put into it /ROOT/html/portlet/login/login.jsp:

<p class="article-img3"></p>
![Liferay](images/06-09-str.png)

We change *jsp* according to requirements:

<p class="article-img3"></p>
![Liferay](images/06-10-hook-jsp.png)

Then we delpoy the hook via *Maven* and check what we have as result:

<p class="article-img3"></p>
![Liferay](images/06-11-h1.png)


