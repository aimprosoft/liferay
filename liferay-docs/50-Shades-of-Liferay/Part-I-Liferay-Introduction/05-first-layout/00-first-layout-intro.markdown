# 5 - First Layout [](id=first-layout)

                                                                                                    Perform all straightly...

Now we will write our first Liferay theme.



Similar to portlet and theme, we will create a new module for layout:

<p class="article-img2"></p>
![Liferay](images/05-01-layout1.png)

<p class="article-img2"></p>
![Liferay](images/05-02-layout2.png)

We get such module structure:

<p class="article-img3"></p>
![Liferay](images/05-03-layout-structure.png)

We will create 1-3-column layout, that has such structure:

<p class="article-img4"></p>
![Liferay](images/05-04-1-3-layout.png)

We will do it on the basis of existing *1_2_columns_i* layout (*file/ROOT/layouttpl/custom/1_2_columns_i.tpl*). Copy content of this file in 1-3-column.tpl. Change layout in oder to there in the buttom were three columns instead of two ones.
Change layout so that in the buttom were three columns instead of two:

<p class="article-img2"></p>
![Liferay](images/05-05-col-1-3.png)

The same is done for the file *1-3-column.wap.tpl*. 

Recompile layout and apply to the required page:

<p class="article-img2"></p>
![Liferay](images/05-06-layout.png)


