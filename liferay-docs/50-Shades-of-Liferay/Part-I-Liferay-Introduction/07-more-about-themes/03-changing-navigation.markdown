# Changing navigation [](id=changing-navigation)

In the classic theme the navigation displays the pages and subpages of the first level. We do so, that the subpages of the second level will be dispalyed in the our theme.

To do that, edit the file *navigation.vm* and add there the nested iteration for passing the subpages of the second level (similarly the iteration for passing the subpages of the first level):

<p class="article-img2"></p>
![Liferay](images/07-15-nav-sub-child.png)

Add also the css-styles for subpages of the second level:

<p class="article-img3"></p>
![Liferay](images/07-16-nav-css.png)

Add some pages/subpages and see what we have as a result:

<p class="article-img2"></p>
![Liferay](images/07-17-nav-display.png)


