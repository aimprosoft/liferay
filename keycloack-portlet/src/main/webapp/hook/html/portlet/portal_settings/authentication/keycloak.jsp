<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/html/portlet/portal_settings/init.jsp" %>

<%
//@note: key names are the same as in 'portal.properties' file
String keyCloackRealmKey            = "keycloak.realm.name";
String keyCloackClientIdKey         = "keycloak.client.id";
String keyCloackClientSecretKey     = "keycloak.secret";
String keyCloackBaseUrlKey          = "keycloak.base.url";
String keyCloackRedirectUrlKey      = "keycloak.redirect.url";
String keyCloackDefaultPassKey      = "keycloak.default.password";


//@note: Use 'com.liferay.portal.kernel.util.*', because 'com.liferay.portal.util.*' is inside portal-impl
String realmName        = com.liferay.portal.kernel.util.PrefsPropsUtil.getString(company.getCompanyId(), keyCloackRealmKey, com.liferay.portal.kernel.util.PropsUtil.get(keyCloackRealmKey));
String clientId         = com.liferay.portal.kernel.util.PrefsPropsUtil.getString(company.getCompanyId(), keyCloackClientIdKey, com.liferay.portal.kernel.util.PropsUtil.get(keyCloackClientIdKey));
String clientSecret     = com.liferay.portal.kernel.util.PrefsPropsUtil.getString(company.getCompanyId(), keyCloackClientSecretKey, com.liferay.portal.kernel.util.PropsUtil.get(keyCloackClientSecretKey));
String baseUrl          = com.liferay.portal.kernel.util.PrefsPropsUtil.getString(company.getCompanyId(), keyCloackBaseUrlKey, com.liferay.portal.kernel.util.PropsUtil.get(keyCloackBaseUrlKey));
String redirectUrl      = com.liferay.portal.kernel.util.PrefsPropsUtil.getString(company.getCompanyId(), keyCloackRedirectUrlKey, com.liferay.portal.kernel.util.PropsUtil.get(keyCloackRedirectUrlKey));
String defaultPassword  = com.liferay.portal.kernel.util.PrefsPropsUtil.getString(company.getCompanyId(), keyCloackDefaultPassKey, com.liferay.portal.kernel.util.PropsUtil.get(keyCloackDefaultPassKey));
    
%>

<aui:fieldset>
	<aui:input cssClass="lfr-input-text-container" label="keycloack-realm-name" name='<%= "settings--" + keyCloackRealmKey + "--" %>' type="text" value="<%= realmName %>" />
	<aui:input cssClass="lfr-input-text-container" label="keycloack-client-id" name='<%= "settings--" + keyCloackClientIdKey + "--" %>' type="text" value="<%= clientId %>" />
	<aui:input cssClass="lfr-input-text-container" label="keycloack-client-secret" name='<%= "settings--" + keyCloackClientSecretKey + "--" %>' type="text" value="<%= clientSecret %>" />
	<aui:input cssClass="lfr-input-text-container" label="keycloack-base-url" name='<%= "settings--" + keyCloackBaseUrlKey + "--" %>' type="text" value="<%= baseUrl %>" />
	<aui:input cssClass="lfr-input-text-container" label="keycloack-redirect-url" name='<%= "settings--" + keyCloackRedirectUrlKey + "--" %>' type="text" value="<%= redirectUrl %>" />
	<aui:input cssClass="lfr-input-text-container" label="keycloack-default-password" name='<%= "settings--" + keyCloackDefaultPassKey + "--" %>' type="text" value="<%= defaultPassword %>" />
</aui:fieldset>