create table KBArticle (
	uuid_ VARCHAR2(75 CHAR) null,
	kbArticleId number(30,0) not null primary key,
	resourcePrimKey number(30,0),
	groupId number(30,0),
	companyId number(30,0),
	userId number(30,0),
	userName VARCHAR2(75 CHAR) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	rootResourcePrimKey number(30,0),
	parentResourceClassNameId number(30,0),
	parentResourcePrimKey number(30,0),
	kbFolderId number(30,0),
	version number(30,0),
	title varchar2(4000) null,
	urlTitle VARCHAR2(75 CHAR) null,
	content clob null,
	description varchar2(4000) null,
	priority number(30,20),
	sections varchar2(4000) null,
	viewCount number(30,0),
	latest number(1, 0),
	main number(1, 0),
	sourceURL varchar2(4000) null,
	status number(30,0),
	statusByUserId number(30,0),
	statusByUserName VARCHAR2(75 CHAR) null,
	statusDate timestamp null
);

create table KBComment (
	uuid_ VARCHAR2(75 CHAR) null,
	kbCommentId number(30,0) not null primary key,
	groupId number(30,0),
	companyId number(30,0),
	userId number(30,0),
	userName VARCHAR2(75 CHAR) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	classNameId number(30,0),
	classPK number(30,0),
	content varchar2(4000) null,
	userRating number(30,0),
	status number(30,0)
);

create table KBFolder (
	uuid_ VARCHAR2(75 CHAR) null,
	kbFolderId number(30,0) not null primary key,
	groupId number(30,0),
	companyId number(30,0),
	userId number(30,0),
	userName VARCHAR2(75 CHAR) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	parentKBFolderId number(30,0),
	name VARCHAR2(75 CHAR) null,
	urlTitle VARCHAR2(75 CHAR) null,
	description varchar2(4000) null
);

create table KBTemplate (
	uuid_ VARCHAR2(75 CHAR) null,
	kbTemplateId number(30,0) not null primary key,
	groupId number(30,0),
	companyId number(30,0),
	userId number(30,0),
	userName VARCHAR2(75 CHAR) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	title varchar2(4000) null,
	content clob null
);
