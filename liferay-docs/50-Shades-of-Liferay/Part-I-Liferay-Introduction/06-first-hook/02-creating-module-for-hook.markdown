# Creating module for hook [](id=creating-module-for-hook)

As previously, we create a new module via *Maven archetype*:

<p class="article-img2"></p>
![Liferay](images/06-01-hook1.png)

<p class="article-img2"></p>
![Liferay](images/06-02-hook2.png)

We will get such structure:

<p class="article-img3"></p>
![Liferay](images/06-03-hook-structure.png)


