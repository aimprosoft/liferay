package com.aimprosoft.keycloack.util;

public interface KeyCloakConstants {
    
    String CODE                     = "code";

    String ACCESS_TOKEN             = "access_token";

    String GRANT_TYPE               = "grant_type";

    String AUTHORIZATION_CODE       = "authorization_code";

    String REDIRECT_URI             = "redirect_uri";

    String CLIENT_ID                = "client_id";

    String CLIENT_SECRET            = "client_secret";


    String EMAIL                    = "email";

    String NAME                     = "name";

    String FAMILY_NAME              = "family_name";

    String PREFERRED_USERNAME       = "preferred_username";

    String GIVEN_NAME               = "given_name";
    

}
